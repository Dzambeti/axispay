
<?php
require 'DB/dbapi.php';
require 'Admin/AdminDB/ODBCAdmin.php';
if (Is_Cust_Logged_In()) {
    $Acc = $_SESSION["Customer"];
    $Details = GetConsumerDetails($Acc);
    $CustomerName = $Details["data"][0]["ConsumerName"];
    $Addr = $Details["data"][0]["ConsumerAddress"];
    $Phone = $Details["data"][0]["ConsumerPhone"];
    $Email = $Details["data"][0]["ConsumerEmail"];
    $IdNum = $Details["data"][0]["ConsumerIDNum"];
    $Balance = $Details["data"][0]["ConsumerBalance"];

    if (empty($Addr)) {
        $Addr = 'Address not found from Promun Master file';
    }
    if (empty($Phone)) {
        $Phone = 'No cell or phone number';
    }
    if (empty($Email)) {
        $Email = 'No Mail found';
    }
    if (empty($IdNum)) {
        $IdNum = 'Not set';
    }

    $BalanceTypes = GetCustomerBalances($Acc);

    $rec_no = date("YmdHis");
} else {
    redirect("index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
        <title>Axis Pay | Base Info</title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- Page plugins css -->
        <link href="plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
        <!-- Color picker plugins css -->
        <link href="plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
        <!-- Date picker plugins css -->
        <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker plugins css -->
        <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>


    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php require 'BaseHeader.php'; ?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Account - <?php echo $Acc; ?></h4> </div>

                        <!-- /.col-lg-12 -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"> Information Overview</div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form">
                                            <div class="form-body">
                                                <h3 class="box-title">Your Info</h3>
                                                <hr class="m-t-0 m-b-40">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Account Number:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static"> <?php echo $Acc; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Account Name:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static"> <?php echo $CustomerName; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Email Address:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static"> <?php echo $Email; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Phone Number:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static"> <?php echo $Phone; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                   

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Address:</label>
                                                            <div class="col-md-8">
                                                                <p class="form-control-static"> <?php echo $Addr; ?> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                    <!--/span-->
                                                </div>
                                                <!--/row-->

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="white-box">
                                <?php 
                                 if(count($BalanceTypes)>0){ ?>
                                <form class="">
                                    <h3 class="box-title">Balance Breakdown</h3>
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <?php
                                        $i = 0;
                                        $TotalDue = 0;
                                       
                                        for ($i = 0; $i < sizeof($BalanceTypes); $i++) {
                                            $TotalDue += array_sum(explode(';', $BalanceTypes[$i]["bal"]));
                                            ?>                       

                                            <?php
                                            if (!empty($BalanceTypes[$i]["type"])) {
                                                //  $TotalDue = $TotalDue + $BalanceTypes[$one]["bal"];
                                                ?>
                                                <div class=" col-lg-3  col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label> <?php
                                        echo @ActBalanceTypeInfo($BalanceTypes[$i]["type"])[0]["bmfDesc"];
                                                ?>

                                                        </label>
                                                        <input type="text" readonly="" style="border-color: black;" class="form-control"  value="<?php echo number_format(round(array_sum(explode(';', $BalanceTypes[$i]["bal"])), 2), 2); ?>"  >

                                                    </div>       
                                                </div> 
                                                <?php
                                            }
                                            //   $i += 1;
                                        }
                                        ?>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3  col-md-12 col-sm-12">
                                            <label>Total</label>
                                            <input type="text" readonly="" style="border-color: black;" class="form-control"  value="<?php echo number_format(round($TotalDue, 2), 2); ?>" required><span class="highlight"></span> <span class="bar"></span>



                                        </div>
                                        <div class="col-lg-3  col-md-12 col-sm-12">
                                            <label>Unprocessed</label>
                                            <?php
                                            @$Unproce = GetTotalUnprocessed($Acc);
                                            @$ValueUnpro = $Unproce[0]["TOT"];
                                            @$grand = $TotalDue - $ValueUnpro;
                                            ?>
                                            <input type="text" readonly="" style="border-color: black;" class="form-control"  value="<?php echo number_format(round($ValueUnpro, 2), 2); ?>" required><span class="highlight"></span> <span class="bar"></span>



                                        </div>

                                        <div class="col-lg-3  col-md-12 col-sm-12">
                                            <label>Grand Total</label>
                                            <input type="text" readonly="" style="border-color: black;" class="form-control"  value="<?php echo number_format(round($grand, 2), 2); ?>" required><span class="highlight"></span> <span class="bar"></span>



                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                                 <div class="row">
                                                     <div class="col-md-offset-3 col-md-9">
                                                         <button type="submit" class="btn btn-success prompt_payment">Make Payment</button>
 
                                                     </div>
                                                 </div>
                                             </div>-->

                                            <div class="col-md-6">
                                                <div class="btn-group m-r-10">
                                                    <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">Make Payment <span class="caret"></span></button>
                                                    <ul role="menu" class="dropdown-menu">
                                                        <li><a class="btn btn-default op" style="color:black">Online Payments</a></li>
                                                        <li><a class="btn btn-default sm" style="color:black">Use Swipe Machine</a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>   







                                </form>
                                <?php  } ?>
                                <div class="row PaymentDiv">
                                    <div class="col-md-6">
                                        <div class="white-box">
                                            <h3 class="box-title m-b-0">Please fill payment details to complete payment</h3> <br>

                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <form>
                                                        <div class="form-group">
                                                            <label for="exampleInputuname">Phone Number (SMS ready)</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class=" ti-android"></i></div>
                                                                <input type="text" style="border-color: black;" class="form-control" id="exampleInputPhone" value="<?php echo $Phone; ?>" placeholder="Mobile Phone "> </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Email address</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class="ti-email"></i></div>
                                                                <input type="email" style="border-color: black;" class="form-control" id="exampleInputEmail1" value="<?php echo $Email; ?>" placeholder="Enter email"> </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputpwd1">Amount to pay</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i class=""> $ </i></div>
                                                                <input type="number" style="border-color: black;" class="form-control" id="AmntPaid" placeholder="Enter amount"> </div>
                                                        </div>

                                                        <div class="paynow_resp"> </div> <br>
                                                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 BtnProcessPayment">Continue...</button>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row SwipePay">
                                <div class="col-md-6">
                                    <div class="white-box">
                                        <h3 class="box-title m-b-0"></h3> <br>

                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <form>

                                                    <div class="form-group">
                                                        <label for="exampleInputpwd1">Amount to pay</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class=""> $ </i></div>
                                                            <input type="number" style="border-color: black;" class="form-control" id="SwipeAmnt" placeholder="Enter amount"> </div>
                                                    </div>

                                                    <div class="swipe_resp"> ..........Processing Transaction......... </div> <br>
                                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 BtnSwipePayment">Proceed...</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>


                    <!-- /.row -->

                </div>


            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="plugins/bower_components/moment/moment.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>


    <script>
        $(document).ready(function () {

            $(".PaymentDiv").hide();
            $(".SwipePay").hide();
            $(".swipe_resp").hide();

            $(".op").click(function (ev)
            {
                ev.preventDefault();
                $(this).hide('slow');
                $(".sm").show();
                $(".PaymentDiv").show('slow');
                $("#exampleInputEmail1").attr("readonly", false);
                $("#exampleInputPhone").attr("readonly", false);
                $("#AmntPaid").attr("readonly", false);
                $(".SwipePay").hide("slow");
            });

            $(".sm").click(function (ev)
            {
                ev.preventDefault();
                $(this).hide('slow');
                $(".PaymentDiv").hide("slow");
                $(".op").show();
                $(".SwipePay").show("slow");
                $("#SwipeAmnt").attr("readonly", false);
            });


            //process payment with paynow  
            $(".BtnProcessPayment").click(function (ev) {
                ev.preventDefault();
                $(".paynow_resp").html(".....Processing payment request, please wait.....");
                var time = '<?php echo "attempt_" . time(); ?>';
                $.get("engines/promunpay.php?state=1&attempt_id=" + time,
                        {
                            AmntPaid: $("#AmntPaid").val(),
                            email: $("#exampleInputEmail1").val(),
                            phone: $("#exampleInputPhone").val()
                        },
                        function (fdbk) {
                            console.log(fdbk);
                            $(".paynow_resp").html(fdbk);

                        });
            });

            //process swipe trsansactions BtnSwipePayment
            //process payment with paynow  
            $(".BtnSwipePayment").click(function (ev) {
                ev.preventDefault();
                $(".swipe_resp").html("...............Processing Request. Please wait........");
                $(".swipe_resp").slideDown("slow");

                $.post("engines/eft.php?case=1",
                        {
                            AmntPaid: $("#SwipeAmnt").val() * 100,
                            rec_no: '<?php echo $rec_no; ?>'

                        },
                        function (fdbk) 
                {
                           
                            var resp = $.parseJSON(fdbk);
                            $(".swipe_resp").html(resp.msg);
                            /* if(resp.msg === "format error"){
                             $.get("engines/") 
                             
                             }*/
                        });
            });

        });

        // Clock pickers
        $('#single-input').clockpicker({
            placement: 'bottom'
            , align: 'left'
            , autoclose: true
            , 'default': 'now'
        });
        $('.clockpicker').clockpicker({
            donetext: 'Done'
            , }).find('input').change(function () {
            console.log(this.value);
        });
        $('#check-minutes').click(function (e) {
            // Have to stop propagation here
            e.stopPropagation();
            input.clockpicker('show').clockpicker('toggleView', 'minutes');
        });
        if (/mobile/i.test(navigator.userAgent)) {
            $('input').prop('readOnly', true);
        }
        // Colorpicker
        $(".colorpicker").asColorPicker();
        $(".complex-colorpicker").asColorPicker({
            mode: 'complex'
        });
        $(".gradient-colorpicker").asColorPicker({
            mode: 'gradient'
        });
        // Date Picker
        jQuery('.mydatepicker, #datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true
            , todayHighlight: true
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
        jQuery('#datepicker-inline').datepicker({
            todayHighlight: true
        });
        // Daterange picker
        $('.input-daterange-datepicker').daterangepicker({
            buttonClasses: ['btn', 'btn-sm']
            , applyClass: 'btn-danger'
            , cancelClass: 'btn-inverse'
        });
        $('.input-daterange-timepicker').daterangepicker({
            timePicker: true
            , format: 'MM/DD/YYYY h:mm A'
            , timePickerIncrement: 30
            , timePicker12Hour: true
            , timePickerSeconds: false
            , buttonClasses: ['btn', 'btn-sm']
            , applyClass: 'btn-danger'
            , cancelClass: 'btn-inverse'
        });
        $('.input-limit-datepicker').daterangepicker({
            format: 'MM/DD/YYYY'
            , minDate: '06/01/2015'
            , maxDate: '06/30/2015'
            , buttonClasses: ['btn', 'btn-sm']
            , applyClass: 'btn-danger'
            , cancelClass: 'btn-inverse'
            , dateLimit: {
                days: 6
            }


        });
    </script>

    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>