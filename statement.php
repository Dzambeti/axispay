<!DOCTYPE html>
<?php 
require 'DB/dbapi.php';
$Acc = $_SESSION["Customer"];
$Details = GetConsumerDetails($Acc);
$CustomerName = $Details["data"][0]["ConsumerName"];
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title>Axispay | Statements</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- wysihtml5 CSS -->
    <link rel="stylesheet" href="plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
    <!-- Dropzone css -->
    <link href="plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.min.css" rel="stylesheet">
                              
    <!-- color CSS -->
    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
        <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <?php require 'BaseHeader.php'; ?> 
        <div id="page-wrapper">
            <div class="container-fluid">
            <form class="floating-labels">
                <div class="row bg-title">
                
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Statement</h4> </div>
                                    </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box printableArea">
                            <h3><b>Water and Services Statement</b> <span class="pull-right">Invoice Number  </span></h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left"> <address>
                                            <h3> &nbsp;<b class="text-danger">Ruwa Local Board</b></h3>
                                            <p class="text-muted m-l-5">Phone :0273 213 2638 /9 ,
  											    <br/>Fax: 0273 213  2838,
                                                <br/> Address : 153 Ruwa ,
                                                </p>
                                        </address> </div>
                                    <div class="pull-right text-right"> <address>
                                            <h3>To:
                                            </h3>
                                            <h4 class="font-bold">T MADZAMBA</h4>
                                            <p class="text-muted m-l-30">7 STAND NO 3959,
                                                <br/>OLD WINDSOR PARK,
                                                
                                            <p class="m-t-30"><b>Last Receipt Date :</b> <i class="fa fa-calendar"></i> 23/02/2017</p>
                                            <p><b>Account Date :</b> <i class="fa fa-calendar"></i> 25/09/2017</p>
                                            </p>
                                        </address> </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Date</th>
                                                    <th>Rep</th>
                                                    <th class="text-right">Details</th>
                                                    <th class="text-right">Amount</th>
                                                                                                   </tr>
                                            </thead>
                                            <tbody>
                                                   <tr>
                                                    <td class="text-center">20/04/2017</td>
                                                    <td>Rates</td>
                                                    <td class="text-right">Refuse </td>
                                                     <td class="text-right"> $1500 </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="pull-right m-t-30 text-right">
                                        <p>Sub - Total amount: $1 500</p>
                                      
                                        <hr>
                                        <h3><b>Total :</b> $1 500</h3> </div>
                                       <table class="table table-hover">
                                        <tbody>
                                                   <tr>
                                                  <td class="text-center"><img src="images/ruwa.png" alt="logo"/></td>
                                                    <td>CREDIT
                                                    <br> 0.00</td>
                                                    <td class="text-right">CURRENT
                                                    <br> 23.70 </td>
                                                     <td class="text-right"> Pay Before 
                                                     <br>
                                             <i class="fa fa-calendar"></i> 21/02/2018  </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <div class="text-right">
                                        <button class="btn btn-danger" type="submit"> Proceed to payment </button>
                                        <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                <!-- /.row -->
             </form>   
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; AxisPay by Axis Solutions</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/tether.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
    <script>
        $(document).ready(function () {
            $('.textarea_editor').wysihtml5();
        });
    </script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>