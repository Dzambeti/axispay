  <!-- Top Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part"><a class="logo" href="home.php"><b><img src="plugins/images/axispay-white.png" alt="home" /></b></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>

                    </ul>
                   
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- End Top Navigation -->
            <!-- Left navbar-header -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <!-- input-group -->
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                                </span> </div>
                            <!-- /input-group -->
                        </li>
                        <li class="user-pro">
                            <a href="home" class="waves-effect"><img src="plugins/images/eliteadmin-logo-dark.png" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo $CustomerName; ?></span>
                            </a>

                        </li>

                        <li> <a href="home" class="waves-effect"><i class="ti-money"></i> <span class="hide-menu"> Pay </span></a></li>
                        <li> <a href="enquire" class="waves-effect"><i class=" icon-cloud-download"></i> <span class="hide-menu"> Enquire</span></a></li>
                       <!-- <li> <a href="statement.php" class="waves-effect"><i class="ti-receipt"></i> <span class="hide-menu">Statement</span></a></li> -->
                        <li> <a href="payments" class="waves-effect"><i class="icon-credit-card"></i> <span class="hide-menu"> Rates / Bills </span></a></li>
                        <li> <a href="nbpayments" class="waves-effect"><i class="icon-basket-loaded"></i> <span class="hide-menu">  Fees / Licenses  </span></a></li>
                        <li> <a href="tranHistory" class="waves-effect"><i class="icon-trophy"></i> <span class="hide-menu">  Account History  </span></a></li>
                       
                        <li><a href="logout?logout=true" class="waves-effect"><i class="icon-logout"></i> <span class="hide-menu">Check out</span></a></li>



                    </ul>
                </div>
            </div>
            <!-- Left navbar-header end -->
            <!-- Page Content -->

