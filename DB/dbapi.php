<?php
session_start();
$db = new PDO('mysql:host=localhost;dbname=laaxpay;charset=utf8', 'root', '');
//$db = new PDO('mysql:host=MYSQL5006.site4now.net;dbname=db_a33c8a_axispay;charset=utf8', 'a33c8a_axispay', 'Axis1991');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);


function logout() {
    session_destroy();
    unset($_SESSION['Customer']);
    return true;
}

function redirect($url) {
    header("Location: $url");
}

function Is_Cust_Logged_In() {
    if (isset($_SESSION['Customer'])) {
        return true;
    }
}

function Is_Logged_In() {
    if (isset($_SESSION['acc'])) {
        return true;
    }
}

function NetworkConnected() {
    $connected = @fsockopen("www.google.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = "yes"; //action when connected
        fclose($connected);
    } else {
        $is_conn = "no"; //action in connection failure
    }
    return $is_conn;
}

//co det
Function GetLADetails(){
   global $db;
    try {

        $sql = $db->prepare(' select * from lamaster');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function GetConsumerDetails($Acc) {

    global $db;
    try {

        $sql = $db->prepare(' select * from customerdata where ConsumerAccount=?');
        $sql->execute(array($Acc));
        $Array = $sql->fetchALL(PDO::FETCH_ASSOC);
        $counter = $sql->rowCount();
        if($counter>0){
          
            $result["status"] = "ok";
            $result["data"] = $Array;
        }
        else{
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;
}

//print_r(GetConsumerDetails(trim(2147483647)));

function GetBalances($Acc) {

    global $db;
    try {

        $sql = $db->prepare(' select * from consumerbalances where AccountNumber=? and OnlineAmountDue>0');
        $sql->execute(array($Acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;
}

function GetBal($Acc) {

    global $db;
    try {

        $sql = $db->prepare(' select sum(OnlineAmountDue) as Tot from consumerbalances where AccountNumber=? and OnlineAmountDue>0');
        $sql->execute(array($Acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;
}

function BalanceTypeInfo($BtypeCode){
  global $db;
    try {

        $sql = $db->prepare(' select * from balancetypemaster where BalanceTypeCode=?');
        $sql->execute(array($BtypeCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;   
}

function ActBalanceTypeInfo($BtypeCode){
  global $db;
    try {

        $sql = $db->prepare(' select * from actualbalancetypes where bmfType=?');
        $sql->execute(array($BtypeCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;   
}


function incomeCodeInfo($incCode){
  global $db;
    try {

        $sql = $db->prepare(' select * from balancetypemaster where IncCode=?');
        $sql->execute(array($incCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;   
}

//print_r(GetBalances($_SESSION["Customer"]));

// paynow integrtion
function LogOrder($phone,$email,$poll_url,$order_id,$amount) {
    global $db;
    try {
        $sql = $db->prepare("insert into attempts(phone,email,poll_url,order_id,amount,AccountNumber,DateCreated,AccRef) values (?,?,?,?,?,?,?,?)");
        $sql->execute(array($phone,$email,$poll_url,$order_id,$amount, $_SESSION["Customer"],date("Y-m-d H:i:s"),$_SESSION["Customer"]));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function LogNonBillOrder($phone,$email,$poll_url,$order_id,$amount,$ref,$optRef,$Address) {
    global $db;
    try {
        $sql = $db->prepare("insert into attempts(phone,email,poll_url,order_id,amount,AccountNumber,DateCreated,AccRef,Address) values (?,?,?,?,?,?,?,?,?)");
        $sql->execute(array($phone,$email,$poll_url,$order_id,$amount, $ref,date("Y-m-d H:i:s"),$optRef,$Address));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}


function GetPollUrl($attempt_id){
       global $db;
 
    try{
        $stm =  $db->prepare("select poll_url from attempts where order_id=?");
        $stm->execute(array($attempt_id));
        $result = $stm->fetchColumn();
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function GetOrderDet($att_id)
{
       global $db;
 
    try{
        $stm =  $db->prepare("select * from attempts where order_id=?");
        $stm->execute(array($att_id));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function LogTransID($transID,$attempt_ID)
        {
   global $db;
    try {
        $sql = $db->prepare("update attempts set TransID=? where order_id=?");
        $sql->execute(array($transID,$attempt_ID));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;   
}

function CreateReceipt($TransactionID,$OrderID,$AmountPaid,$Phone,$Email,$PaynowRef) 
       {
    global $db;
    try {
        $sql = $db->prepare("insert into reciepts(RecCode,TransactionID,OrderID,AmountPaid,AccountNumber,AccRef,Phone,Email,DateCreated,PaynowRef) values (?,?,?,?,?,?,?,?,?,?)");
        $sql->execute(array("ZZ",$TransactionID,$OrderID,$AmountPaid, $_SESSION["Customer"],$_SESSION["Customer"],$Phone,$Email,date("Y-m-d H:i:s"),$PaynowRef));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function CreateExceptReceipt($TransactionID,$OrderID,$AmountPaid,$AccNum,$AccRef,$Phone,$Email,$PaynowRef) 
       {
    global $db;
    try {
        $sql = $db->prepare("insert into reciepts(RecCode,TransactionID,OrderID,AmountPaid,AccountNumber,AccRef,Phone,Email,DateCreated,PaynowRef) values (?,?,?,?,?,?,?,?,?,?)");
        $sql->execute(array("ZZ",$TransactionID,$OrderID,$AmountPaid,$AccNum,$AccRef,$Phone,$Email,date("Y-m-d H:i:s"),$PaynowRef));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

//echo base64_decode("L1BheW1lbnQvQ2FuY2VsUGF5bWVudC8zMTY0NTMz0");




function CreateNonBillReceipt($RecCode,$TransactionID,$OrderID,$AmountPaid,$ledger,$accref,$Address,$Phone,$Email,$PaynowRef) 
       {
    global $db;
    try {
        $sql = $db->prepare("insert into reciepts(RecCode,TransactionID,OrderID,AmountPaid,AccountNumber,AccRef,Address,Phone,Email,DateCreated,PaynowRef) values (?,?,?,?,?,?,?,?,?,?,?)");
        $sql->execute(array($RecCode,$TransactionID,$OrderID,$AmountPaid, $ledger,$accref,$Address,$Phone,$Email,date("Y-m-d H:i:s"),$PaynowRef));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function GetReceiptDetails($TransID){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where TransactionID=?");
        $stm->execute(array($TransID));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}
function GetAllRdyRecs(){
     global $db;
 
    try{
        $stm =  $db->prepare("select sum(AmountPaid) as RecTot, AccountNumber, TransactionID from reciepts where MunRctSyncStatus is NULL and MunRctCtrSyncStatus is NULL group by AccountNumber");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}






function GetReceiptTransactions($TransID){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from settlementsmaster where Reference=?");
        $stm->execute(array($TransID));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function GetReceipts(){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where AccountNumber=?");
        $stm->execute(array($_SESSION["Customer"]));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function GetNBReceipts($Phone, $AccRef){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where (AccRef=? or AccRef = ?) and AccountNumber <> ?");
        $stm->execute(array($AccRef,$Phone,$AccRef));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function GetReceiptsGivenAcc($AccNum){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where AccountNumber=?");
        $stm->execute(array($AccNum));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function getAccTot($AccNum){
   global $db;
 
    try{
        $stm =  $db->prepare("select sum(AmountPaid) as Tot from reciepts where AccountNumber=?");
        $stm->execute(array($AccNum));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;   
}
function getReceiptsToSync(){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where SyncStatus is NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function AllocatePaymnt($Amnt,$AccNum,$BalTyp){
    global $db;
    try {
        $sql = $db->prepare("update consumerbalances set AmntPaid=AmntPaid+? where AccountNumber=? and BalType=?");
        $sql->execute(array($Amnt,$AccNum,$BalTyp));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result; 
}


function UploadAlloc($BalType,$ReceiptCode,$AmntPaidFor,$Reference){
    global $db;
    try {
        $sql = $db->prepare("insert into settlementsmaster(BalType,ReceiptCode,AmntPaidFor,Reference,Account,DateCreated) values (?,?,?,?,?,?)");
        $sql->execute(array($BalType,$ReceiptCode,$AmntPaidFor,$Reference,$_SESSION["Customer"],date("Y-m-d H:i:s")));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

Function SetNewBalance($Amnt,$AccNum,$BalTyp){
 global $db;
    try {
        $sql = $db->prepare("update consumerbalances set OnlineAmountDue=OnlineAmountDue-? where AccountNumber=? and BalType=?");
        $sql->execute(array($Amnt,$AccNum,$BalTyp));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;    
}


function get_pos_responses($wezhaz){
  global $db;
    try{
    $sql = "SELECT * FROM pos_response where response_code = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($wezhaz));
    $rslt = $stmt->fetchAll(PDO::FETCH_ASSOC);
   
    } catch (Exception $ex) {
        $rslt=$ex->getMessage();
    }
    return $rslt;      
}

function get_swipe_countdown(){
     global $db;
    try{
    $sql = "SELECT seconds FROM swipe_countdown";
    $stmt = $db->prepare($sql);
    $stmt->execute(array());
    $rslt = $stmt->fetchAll(PDO::FETCH_ASSOC);
   
    } catch (Exception $ex) {
        $rslt=$ex->getMessage();
    }
    return $rslt;   
}
//company details

function getVoteTypes()
{
    global $db;
    try {

        $sql = $db->prepare(" select * from balancetypemaster where IncType = 'V' ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

//echo base64_decode("L1BheW1lbnQvQ2FuY2VsUGF5bWVudC8zMTUxNDI00");


function updateAttemptStatus($TransStatus,$OrderID){
    global $db;
    try {
        $sql = $db->prepare("update attempts set transStatus = ? where order_id = ?");
        $sql->execute(array($TransStatus,$OrderID));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) 
    {
        $result["status"] = $ex->getMessage();
    }
    return $result;   
}



function getNonAttempts(){
     global $db;
 
    try{
       
        $stm =  $db->prepare("SELECT * FROM `attempts` WHERE transStatus IS NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function DeleteBaCustData(){
    global $db;
   try {
          
       $sql = $db->prepare('delete from customerdata');  
       $sql_alt = $db->prepare('ALTER TABLE customerdata AUTO_INCREMENT=1');    
       $sql->execute();
       $sql_alt->execute();
      $count = $sql->rowCount();
       if ($count > 0) {
           $result["status"] = "ok";
       } else {
           $result["status"] = "fail";
       }
     
   } catch (Exception $ex) {
       $result["status"] = $ex->getMessage();
   }
   return $result;
}

function uploadImport($Acc, $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail, $ConsumerIDNum,$ConsumerBalance,$ConsumerOnlineBalance,$CurrentBal,$LastSyncBy){
    global $db;
    try {
      //if not insert into db
        $sql = $db->prepare('insert into customerdata (ConsumerAccount,ConsumerName,ConsumerAddress,ConsumerPhone,ConsumerEmail,ConsumerIDNum,ConsumerBalance,ConsumerOnlineBalance,CurrentBal,LastSyncBy,LastSync) values(?,?,?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($Acc, $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail, $ConsumerIDNum,$ConsumerBalance,$ConsumerOnlineBalance,$CurrentBal,$LastSyncBy,date("Y-m-d H:i:s")));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "error";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}