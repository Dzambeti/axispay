<?php

session_start();

/***************************************FUNCTIONS TO INSERT INTO WEB DATABASE*********************/

$db = new PDO('mysql:host=localhost;dbname=laaxpay;charset=utf8', 'root', '');
//$db = new PDO('mysql:host=MYSQL5006.site4now.net;dbname=db_a33c8a_axispay;charset=utf8', 'a33c8a_axispay', 'Axis1991');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

function redirect($url) {
    header("Location: $url");
}

function logout() {
    session_destroy();
    unset($_SESSION['acc']);
    return true;
}

function Is_Logged_In() {
    if (isset($_SESSION['acc'])) {
        return true;
    }
}

function AdminLogin($username, $password) {
    global $db;
    try {

        $sql = $db->prepare('select * from admins where (UserCode=? or Username=?) and password=?');
        $sql->execute(array($username, $username, $password));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $_SESSION['acc'] = $result[0]['id'];
            $_SESSION['UserCode'] = $result[0]['UserCode'];
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
    }

    return $status;
}

function CreateAdmin($UserCode,$Username,$Phone){
  global $db;
    try {
      //if not insert into db
        $Password = "12345";
        $sql = $db->prepare('insert into admins (UserCode,Username,Password,Phone,DateCreated) values(?,?,?,?,?)');
        $sql->execute(array($UserCode,$Username,$Password,$Phone,date("Y-m-d H:i:s")));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "error";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;  
}

function UploadRatePayers($Acc, $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail,$ConsumerIDNum,$ConsumerBalance,$LastSyncBy) {
    global $db;
    try {
      //if not insert into db
        $sql = $db->prepare('insert into customerdata (ConsumerAccount,ConsumerName,ConsumerAddress,ConsumerPhone,ConsumerEmail,ConsumerIDNum,ConsumerBalance,LastSyncBy,LastSync) values(?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($Acc, $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail, $ConsumerIDNum,$ConsumerBalance,$LastSyncBy,date("Y-m-d H:i:s")));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "error";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function UpdateRatePayers($Acc, $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail,$ConsumerIDNum,$ConsumerBalance,$LastSyncBy) {
    global $db;
    try {
      //if not insert into db
        $sql = $db->prepare('update customerdata set ConsumerName=?,ConsumerAddress=?,ConsumerPhone=?,ConsumerEmail=?,ConsumerIDNum=?,ConsumerBalance=?,LastSyncBy=? where ConsumerAccount=?');
        $sql->execute(array( $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail, $ConsumerIDNum,$ConsumerBalance,$LastSyncBy,$Acc));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "error";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function GetCustomerDetails($CustomerIdentifier)
{
    global $db;
    try {

        $sql = $db->prepare(' select * from customerdata where (ConsumerAccount = ? or ConsumerID=?)');
        $sql->execute(array($CustomerIdentifier,$CustomerIdentifier));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function GetCustomers()
{
    global $db;
    try {

        $sql = $db->prepare('select * from customerdata');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function HowMuchAccPaid($acc)
{
    global $db;
    try {

        $sql = $db->prepare('select sum(AmountPaid) as Paid from reciepts where AccountNumber=?');
        $sql->execute(array($acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}




function UploadLaDet($LAName,$LaCode,$Address,$VATNum,$CentralLandLine,$CoEmail,$CoPhone)
{
     global $db;
     try{
         $IsInPilot = 'yes';
         $PilotDueDate = date("Y-m-d H:i:s",strtotime("+ 30 Days"));
         $smsCreds = 10;
         $sql = $db->prepare("insert into lamaster(LAName,LaCode,Address,VATNum,CentralLandLine,CoEmail,CoPhone,DateCreated,SMSCreds,IsInPilot,PilotDueDate) values(?,?,?,?,?,?,?,?,?,?,?)");
         $sql->execute(array($LAName,$LaCode,$Address,$VATNum,$CentralLandLine,$CoEmail,$CoPhone,date("Y-m-d H:i:s"),$smsCreds,$IsInPilot,$PilotDueDate));
         if($sql->rowCount()>0)
             {
             $rslt["status"] = "ok";
         }
         else{
              $rslt["status"] = "error";
         }
     } 
     catch (Exception $ex) 
     {
            $rslt["status"] = $ex->getMessage();
     }
     return $rslt;
}

function UpdateLaDet($LAName,$LaCode,$Address,$VATNum,$CoEmail,$opcode,$mcno,$PaynowIntKey, $PaynowIntID,$returnURL, $id)
{
     global $db;
     try{
         $sql = $db->prepare("update lamaster set LAName=?,LaCode=?,Address=?,VATNum=?,CoEmail = ?, OpCode = ?, McNo = ?,PaynowIntKey = ?, PaynowIntID = ?, returnURL = ?, DateCreated = ? where id = ?");
         $sql->execute(array($LAName,$LaCode,$Address,$VATNum,$CoEmail,$opcode,$mcno,$PaynowIntKey, $PaynowIntID, $returnURL,date("Y-m-d H:i:s"),$id));
         if($sql->rowCount()>0)
             {
             $rslt["status"] = "ok";
         }
         else{
              $rslt["status"] = "error";
         }
     } 
     catch (Exception $ex) 
     {
            $rslt["status"] = $ex->getMessage();
     }
     return $rslt;
}

function UploadUserDet($UserCode,$Username,$Password,$Phone){
     global $db;
     try{
         $sql = $db->prepare("insert into admins(UserCode,Username,Password,Phone,DateCreated) values(?,?,?,?,?)");
         $sql->execute(array($UserCode,$Username,$Password,$Phone,date("Y-m-d H:i:s")));
         if($sql->rowCount()>0){
             $rslt["status"] = "ok";
         }
         else{
              $rslt["status"] = "error";
         }
     } catch (Exception $ex) {
            $rslt["status"] = $ex->getMessage();
     }
     return $rslt;
}

function GettAdminInfo($id){
      global $db;
    try {

        $sql = $db->prepare('select * from admins where id = ?');
        $sql->execute(array($id));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function edit_user($username, $userfirstmame, $user_phone, $user_ID) {
    global $db;
    $result = array();
    try {
        $sql = $db->prepare('update admins set UserCode=?,Username=?,Phone=? where id=?');
        $sql->execute(array($username, $userfirstmame, $user_phone, $user_ID));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function get_all_users_expt_this($UserID) {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from admins where id !=?');
        $sql->execute(array($UserID));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

//get balance type
function UploadBalTypes($IncCode,$IncomeCodeDesc,$IncType,$LedgerAcc,$BalanceTypeCode,$LinkedToAcc) {
    global $db;
    try {
     
        $sql = $db->prepare('insert into balancetypemaster (IncCode,IncomeCodeDesc,IncType,LedgerAcc,BalanceTypeCode,LastSyncDate,LinkedToAcc) values(?,?,?,?,?,?,?)');
        $sql->execute(array($IncCode,$IncomeCodeDesc,$IncType,$LedgerAcc,$BalanceTypeCode,date("Y-m-d H:i:s"),$LinkedToAcc));
         $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "error";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function updateLink($LinkedToAcc, $id) {
    global $db;
    try {
     
        $sql = $db->prepare('update balancetypemaster set LinkedToAcc = ? where ID=?');
        $sql->execute(array($LinkedToAcc, $id));
         $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "error";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function DelBtypes(){
     global $db;
    try {
        
        $sql = $db->prepare('delete from balancetypemaster');
        $sql_alt = $db->prepare('ALTER TABLE balancetypemaster AUTO_INCREMENT=1'); 
        $sql->execute();
         $sql_alt->execute();
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function GetBTypes($BTIDCode)
{
    global $db;
    try {

        $sql = $db->prepare(' select * from balancetypemaster where (BalanceTypeCode = ? or BalanceTypeID=?)');
        $sql->execute(array($BTIDCode,$BTIDCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}



function UploadCustomerBalances($Acc,$BalType,$AmtDue) {
    global $db;
    try {
        $_SESSION["acc"] = 1;
     
        $sql = $db->prepare('insert into consumerbalances (AccountNumber,BalType,AmountDue,OnlineAmountDue,LastPostBy,DatePosted) values(?,?,?,?,?,?)');
        $sql->execute(array($Acc,$BalType,$AmtDue,$AmtDue,$_SESSION["acc"],date("Y-m-d H:i:s")));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

//ALTER TABLE `table_name` AUTO_INCREMENT=1
function DeleteBalTypes(){
     global $db;
    try {
           
        $sql = $db->prepare('delete from consumerbalances');  
        $sql_alt = $db->prepare('ALTER TABLE consumerbalances AUTO_INCREMENT=1');    
        $sql->execute();
        $sql_alt->execute();
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function DeleteBalCustData(){
     global $db;
    try {
           
        $sql = $db->prepare('delete from customerdata');  
        $sql_alt = $db->prepare('ALTER TABLE customerdata AUTO_INCREMENT=1');    
        $sql->execute();
        $sql_alt->execute();
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}


function GetBalancesData($Acc,$Btype)
{
    global $db;
    try {

        $sql = $db->prepare(' select * from consumerbalances where AccountNumber = ? and BalType=?)');
        $sql->execute(array($Acc,$Btype));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

Function GetLADetails(){
   global $db;
    try {

        $sql = $db->prepare(' select * from lamaster');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

Function GetUserDetails(){
   global $db;
    try {

        $sql = $db->prepare(' select * from admins');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function CreateCoDetails($CoName,$Address,$VATNum,$LinkNum,$CoEmail,$CoPhone){
    global $db;
    try {
        $sql = $db->prepare('insert into lamaster (LAName,Address,VATNum,LinkNum,CoEmail,CoPhone,DateCreated) values(?,?,?,?,?,?,?)');
        $sql->execute(array($CoName,$Address,$VATNum,$LinkNum,$CoEmail,$CoPhone,date("Y-m-d H:i:s")));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function UpdateCoDetails($CoName,$Address,$VATNum,$LinkNum,$CoEmail,$CoPhone){
    global $db;
    try {
        $sql = $db->prepare('update lamaster set LAName = ? ,Address = ?,VATNum = ?,LinkNum,CoEmail,CoPhone,DateCreated) values(?,?,?,?,?,?,?)');
        $sql->execute(array($CoName,$Address,$VATNum,$LinkNum,$CoEmail,$CoPhone,date("Y-m-d H:i:s")));
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function GetOpenTransactions(){
     global $db;
    try {

        $sql = $db->prepare(' select * from settlementsmaster where SyncStatus=0');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function GetReceipts(){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function getReceiptsToSync(){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where MunRctSyncStatus is NULL and MunRctCtrSyncStatus is NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function recsWithNoMunrctctr(){
     global $db;
 
    try{
        $stm =  $db->prepare("select * from reciepts where MunRctSyncStatus is not NULL and MunRctCtrSyncStatus is NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function GetAttempts(){
     global $db;
 
    try{
       
        $stm =  $db->prepare("SELECT * FROM `attempts` WHERE TransID IS NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function getNullAttempts(){
     global $db;
 
    try{
       
        $stm =  $db->prepare("SELECT * FROM `attempts` WHERE transStatus IS NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function getNonAttempts(){
     global $db;
 
    try{
       
        $stm =  $db->prepare("SELECT * FROM `attempts` WHERE transStatus IS NULL");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}


function GetBtypz(){
     global $db;
 
    try{
       
        $stm =  $db->prepare("SELECT * FROM `balancetypemaster`");
        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function incomCodeInfo($incCode){
  global $db;
    try {

        $sql = $db->prepare(' select * from balancetypemaster where IncCode=?');
        $sql->execute(array($incCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;   
}

function GetBtypIncome($BalTtype){
     global $db;
 
    try{
       
        $stm =  $db->prepare("SELECT sum(AmntPaidFor) as tot FROM `settlementsmaster` where BalType=?");
        $stm->execute(array($BalTtype));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function incomeCodeInfoTwo($incCode){
  global $db;
    try {

        $sql = $db->prepare(' select * from balancetypemaster where IncCode=?');
        $sql->execute(array($incCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
           } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }

    return $result;   
}

function UpdateSyncStatus($MunRctSyncStatus,$MunRctCtrSyncStatus,$recno){
     global $db;
 
    try{
        $stm =  $db->prepare("update reciepts set MunRctSyncStatus=? , MunRctCtrSyncStatus=? where TransactionID=?");
        $stm->execute(array($MunRctSyncStatus,$MunRctCtrSyncStatus,$recno));
         $count = $stm->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
        
    } catch (Exception $ex) {
               $result["status"]=$ex->getMessage();
    }
    
    return $result;
}

function UpdateAccSyncStatus($MunRctSyncStatus,$MunRctCtrSyncStatus,$Acc){
     global $db;
 
    try{
        $stm =  $db->prepare("update reciepts set MunRctSyncStatus=? , MunRctCtrSyncStatus=? where AccountNumber=?");
        $stm->execute(array($MunRctSyncStatus,$MunRctCtrSyncStatus,$Acc));
       $count = $stm->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
        
    } catch (Exception $ex) {
               $result["status"]=$ex->getMessage();
    }
    return $result;
}



