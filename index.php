<!DOCTYPE html>
<?php
require 'DB/dbapi.php';
?>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
        <title>Axis Pay </title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <div class="preloader">
            <div class="cssload-speeding-wheel">test</div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box login-sidebar">

                <div class="white-box">

                    <a href="javascript:void(0)" class="text-center db imgView"><img src="plugins/images/eliteadmin-logo-dark.png" alt="Home" />
                        <br/><img src="plugins/images/axispay.png" alt="Home" /></a>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <label class="">Choose Pay Action:</label>
                            <select class="form-control" type="text" name="paytype" id="paytype" required="" data-placeholder="Choose Pay option">
                                <option value="">Choose Pay type</option>
                                <option value="bill">Pay / Inquire my account</option>
                                <option value="nobill">Pay Fees and Licenses</option>
                            </select>
                        </div>
                    </div>



                    <div class="nonbill">

                        <form class="form-horizontal form-material" id="" method="post" action="">

                            <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <label class="">Choose Pay Code:</label>
                                    <select class="form-control" type="text" name="paytypeval" id="paytypeval" required="" data-placeholder="Choose Pay option">
                                        <option value="">Choose Pay Code</option>
                                        <?php
                                        $pcodes = getVoteTypes();
                                        foreach ($pcodes as $pc) {
                                            $recCode = $pc["IncCode"];
                                            $recDesc = $pc["IncomeCodeDesc"];
                                            ?>
                                            <option value="<?php echo $recCode; ?>"><?php echo $recDesc . " ($recCode)"; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>



                            <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" name="AccNum" id="accNumRef" required="" placeholder="Enter Name / Account Number"> </div>
                            </div>
                            
                             <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" name="Address" id="Address" required="" placeholder="Enter  Physical Address"> </div>
                            </div>

                            <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <input class="form-control" type="phone" name="phone" id="myPhone" required="" placeholder="Enter  Your Phone Number"> </div>
                            </div>
                            
                              <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <input class="form-control" type="Email" name="Email" id="Email" required="" placeholder="Enter  Your Email Address"> </div>
                            </div>
                            
                            

                            <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <input class="form-control" type="number" name="amount" id="myAmount" required="" placeholder="Enter Amount"> </div>
                            </div>

                            <div class ="paynow_resp">
                            </div>
                            <div class="form-group text-center m-t-20">
                                <div class="col-xs-12">
                                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light btnProcessNonBillable" type="" name="">Process...</button>
                                </div>
                            </div>




                        </form>
                    </div>

                    <div class="account">

                        <form class="form-horizontal form-material" id="loginform" method="post" action="">

                            <div class="form-group m-t-40">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" name="AccNum" id="AccNum" required="" placeholder="Account Number"> </div>
                            </div>


                            <div class="form-group text-center m-t-20">
                                <div class="col-xs-12">
                                    <button class="btn btn-info btn-lg btn-block  waves-effect waves-light btnCheckIn" type="submit" name="checkin">Check in</button>
                                </div>
                            </div>
                            <div class="respo">

                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </section>
        <!-- jQuery -->
        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

        <script>
            $(document).ready(function () {
                $(".account").hide();
                $(".nonbill").hide();

                $("#paytype").on("change", function (ev) {
                    ev.preventDefault();
                    var choice = $(this).val();
                    if (choice === "bill")
                    {
                        $(".account").show("slow");
                        $(".nonbill").hide("slow");
                         $(".imgView").show("slow");
                    } else if (choice === "nobill") {
                        $(".account").hide("slow");
                        $(".nonbill").show("slow");
                        $(".imgView").hide("slow");
                    } else {
                        $(".account").hide("slow");
                        $(".nonbill").hide("slow");
                          $(".imgView").show("slow");
                    }
                });

                $(".btnCheckIn").click(function (ev) {
                    ev.preventDefault();
                    $(".respo").html("....Checking account details, please wait....");
                    $.post("engines/checkIn.php",
                            {
                                accNum: $("#AccNum").val()
                            },
                            function (resp) {

                                console.log(resp);
                                var fdbk = $.parseJSON(resp);

                                if (fdbk.status === "ok") {
                                    window.location = fdbk.msg;
                                } else if (fdbk.status === "notok") {
                                    $(".respo").html(fdbk.msg);
                                }
                            });
                });


                //process payment with paynow   for non billable
                $(".btnProcessNonBillable").click(function (ev) {
                    ev.preventDefault();

                    var time = '<?php echo "attempt_" . time(); ?>';
                    $.get("engines/payNonBillables.php?state=1&attempt_id=" + time,
                            {
                                AmntPaid: $("#myAmount").val(),
                                accRef: $("#accNumRef").val(),
                                incCode: $("#paytypeval").val(),
                                phone: $("#myPhone").val(),
                                Email: $("#Email").val(),
                                Address: $("#Address").val()
                            },
                            function (fdbk) {
                                console.log(fdbk);
                                $(".paynow_resp").html(fdbk);

                            });
                });

            });
        </script>
    </body>

</html>