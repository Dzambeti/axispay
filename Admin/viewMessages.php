<?php 
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
 $Users = GetUsers();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title>Axispay | Messages</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- wysihtml5 CSS -->
    <link rel="stylesheet" href="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
    <!-- Dropzone css -->
    <link href="../plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
     <link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="../css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/style.min.css" rel="stylesheet">
                              
    <!-- color CSS -->
    <link href="../css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
        <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        
      <?php  require 'header.php'; ?>
        
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">All Messages</h4> </div>
                        
                   
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                           <br>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Sent Time</th>
                                            <th>Message Body</th>
                                             <th>Delivery Status</th>
                                             <th>Credits</th>
                                         
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                       $msgs = showMessages();
                                        foreach($msgs as $sms){
                                         
                                            $from = $sms["msg_from"];
                                            $to = $sms["msg_to"];
                                            $delvstatus = $sms["status"];
                                            $credits = $sms["Total_sms"];
                                            $sentBy = $sms["user"];
                                            $bdy = $sms["body"];
                                            
                                              $datesent   = date("d M y",  strtotime($sms["sentTime"]));
                                             
                                              
                                        ?>
                                        <tr>
                                            <td><?php echo $from; ?></td>
                                            <td><?php echo $to; ?></td>
                                            <td><?php echo $datesent; ?></td>
                                            <td><?php echo $bdy; ?></td>
                                            <td><?php echo $delvstatus; ?></td>
                                            <td><?php echo $credits; ?></td>
                                         
                                           
                                           </tr>
                                        <?php } ?>
                            
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- /.row -->
                
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; AxisPay by Axis Solutions</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bootstrap/dist/js/tether.min.js"></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
     <script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <script src="../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="../plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
    <script>
        $(document).ready(function () {
            $('.textarea_editor').wysihtml5();
             $('#myTable').DataTable();
             $(".slider").hide();
             
             $(".SyncBalances").click(function(ev){
                 ev.preventDefault();
                 $(this).prop("disabled",true);
                 $(".slider").slideDown(1000);
                 $.get("adminengines/GetCustomers.php?state=one",function(resp){
                     //console.log(resp);
                    var jsnFbk = $.parseJSON(resp);
                     if(jsnFbk.status==="ok")
                     {
                         $(".slider").html("Done synchronising data. Page now reloading...."); 
                        // $(".slider").slideUp("slow"); 
                          var delay = 3000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                     }
                     else
                     {
                          $(".slider").html(jsnFbk.msg); 
                          $(".slider").slideUp("slow"); 
                     } 
                     
                 });
                 
                 
             });
             
          
                 
                 
             });
      
    </script>
    <!-- Custom Theme JavaScript -->
    <script src="../js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>