<?php

@ob_start();
@session_start();
require_once('../AdminDB/mesgAPI.php');
require_once('../AdminDB/DBAPI.php');

//include_once('libs/httpful.phar');

class Message implements JsonSerializable {

    private $to;
    private $from;
    private $sentTime;
    private $receivedTime;
    private $body;
    private $id;
    private $acc;
    private $user;
    private $status;
    private $delivered;
    private $validity;
    private $setFlow;
    private $varflowlevel;

    function __construct($id, $to, $from, $sentTime, $receivedTime, $body) {
        $this->to = $to;
        $this->from = $from;
        $this->sentTime = $sentTime;
        $this->receivedTime = $receivedTime;
        $this->body = $body;
        $this->id = $id;
        //$this->acc = $_SESSION['acc'];
        $this->user = $_SESSION['acc'];
        $this->status = 'submitted';
    }

    function getTo() {
        return $this->to;
    }

    function setTo($to) {
        $this->to = $to;
    }

    function getFrom() {
        return $this->from;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function getSentTime() {
        return $this->sentTime;
    }

    function setSentTime($sentTime) {
        $this->sentTime = $sentTime;
    }

    function getReceivedTime() {
        return $this->receivedTime;
    }

    function setReceivedTime($receivedTime) {
        return $this->receivedTime = $receivedTime;
    }

    function getBody() {
        return $this->body;
    }

    function setBody($body) {
        return $this->body = $body;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        return $this->id = $id;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getStatus() {
        return $this->status;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function getUser() {
        return $this->user;
    }

    function setValidity($validity) {
        $this->validity = $validity;
    }

    function isValid() {
        return $this->validity;
    }

    function setFlow($flow) {
        $this->setFlow = $flow;
    }

    function setFlowLevel($level) {
        $this->varflowlevel = $level;
    }

  

    function save() {
        $credit = ceil(strlen($this->getBody()) / 160);
        saveToSendMsg($this, $credit);
        $file = fopen("msgoutlog.txt", "a");
        fclose($file);
    }

    function getKeyWord() {
        //trim whitespace from the ends of the string first

        $spaceFree = trim($this->body);

        //find the 1st word wc is the keyword
        return strtok($spaceFree, " "); //
    }

    function logMsgToFile() {
        return $this->id . "\t" . $this->from . "\t" . $this->to . "\t" . $this->body . "\t" . $this->sentTime . "\t" . $this->receivedTime . PHP_EOL;
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    function setAcc($acc) {
        $this->acc = $acc;
    }

    function getAcc() {
        return $this->acc;
    }

    function getDelivered() {
        return $this->delivered;
    }

    function setDelivered($delivrd) {
        $this->delivered = $delivrd;
    }

    function update() {
        updateMsg($this);
    }

    
function sendUserBulk($senderId,$groupid,$lastGrpInserID)
{

         
            $bal_credit = getBalance();
            $bal = $bal_credit[0]["creds"];

            $credit = ceil(strlen($this->getBody())/160);
            
            if($bal>0 && $bal>=$credit){

            $lstid = saveToSendMsg($this,$groupid,$credit);
            //IF GROUpd id is not null, update grpup and add credits
            if($groupid!=NULL){
            //QUERY TO add to group credtis
            addToSendGroupSms($credit,$lastGrpInserID);

            }
            reduceToSendCredits($credit);
            $sendSMS = $this->sendUserSms($senderId,$lstid);
            if($sendSMS['status'] == "ok"){
            $result["status"] = "ok";
            $result["sms"] = $sendSMS["msg"];
            }
            else{
                $result["sms"] = '<div class="alert alert-danger">'.$sendSMS['msg'].'</div> ';
                $result["status"] = "off";
            
            }
            }
            else {

            $result["status"] = "off";
                        $result["sms"] = '<div class="alert alert-danger">credits not enough to send to selected contacts, please top up credits </div>';
            }
            return $result;
}


//save user message



function sendUserSms($msg_from,$lstinstId)
{

  
  if(strpos(trim($this->to),"0") === 0)
{

            $recepient="263".substr($this->to,1);

}
       elseif(strpos(trim($this->to),"7") === 0)
{

            $recepient="2637".substr($this->to,1);

}
else if(strpos(trim($this->to),"+") === 0)
{
$recepient=substr($this->to,1);
                                            
}
       elseif (strpos(trim($this->to),"2") === 0) {
            $recepient=$this->to;
   }

$message = urlencode($this->body);
//  $url = "http://rest.bluedotsms.com/api/SendSMS?api_id=API600823199&api_password=1234567890&sms_type=P&encoding=T&sender_id=$msg_from&phonenumber=$recepient&textmessage=$message";

//         $response = file_get_contents($url);

$params = [
   'api_id' => 'API600823199',
   'api_password' => '1234567890',
   'sender_id'   => $msg_from,
   'sms_type' => 'P',
   'encoding' => 'T',
   'phonenumber' => $recepient,
   'textmessage'   => $this->body,
];

$encoded_params = array();

   foreach ($params as $k => $v){

       $encoded_params[] = urlencode($k).'='.urlencode($v);
   }


$url = "http://rest.bluedotsms.com/api/SendSMS?".implode('&', $encoded_params);

// print_r($url);
// die();
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   $response = curl_exec($ch);
   curl_close($ch);



       $json = json_decode($response, TRUE); //generate array object from the response from the web
    
       if ($json['status'] == "S") {
           $messageID = $json["message_id"]; //message id for the check delivery API
           updateToSendMSG($messageID, $lstinstId);
$status["msg"] ="Message send to $recepient with message id $messageID";
$status["status"] = "ok";

//echo $recepient;
//die();
       } else {
           $status["status"] = "fail";
           $status["msg"] = "Failed to send message with status : ".$json['remarks'];
           //print_r($json);
       }
       return $status;

}

//interactions **********************************************************


    function ReplySMS($lstinstId) {
        if (strpos(trim($this->getFrom()), "0") === 0) {

            $recepient = "263" . substr($this->getFrom(), 1);
        } elseif (strpos(trim($this->getFrom()), "7") === 0) {

            $recepient = "263" . substr($this->getFrom(), 1);
        } else if (strpos(trim($this->getFrom()), "+") === 0) {
            $recepient = substr($this->getFrom(), 1);
        } elseif (strpos(trim($this->getFrom()), "2") === 0) {
            $recepient = $this->getFrom();
        }
      
        $message = urlencode($this->getBody());
        
        $url = "http://api.bluedotsms.com/api/mt/SendSMS?user=Axis&password=1234567890&senderid=".$this->getTo()."&channel=Normal&DCS=0&flashsms=0&number=".$recepient."&text=".$message;

    $response = file_get_contents($url);
  
        $json = json_decode($response, TRUE); //generate array object from the response from the web
        if ($json['ErrorCode'] == "000" && $json['ErrorMessage'] == "Done") {
            $messageID = $json["JobId"]; 
            updateToSendMSG($messageID, $lstinstId);
        } else {

            //print_r($json);
        }
        //return json_encode($json);
    }

    function SendAndSaveMSG() {
        $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($this->getBody()) / 160);

        if ($bal >= $credit) {
            $result["status"] = "ok";
       $lstid = saveRplyFormMsg($this, $credit);
                 reduceToSendCredits($credit);
            $this->ReplySMS($lstid);
        } else {

            $result["status"] = "off";
        }
        return $credit;
    }

}
