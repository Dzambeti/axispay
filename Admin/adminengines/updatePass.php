<?php
require '../AdminDB/DBAPI.php';
$old_pass = $_POST['oldpass'];
$new_pass = $_POST['newpass'];
$confirm = $_POST['confirmpass'];
$user_id = $_SESSION['acc'];


if(strlen($new_pass)<5 ){
    $result['msg']='New password: Please make sure the password length is greater or equal to 4 characters.';
    $result['log']='fail';  
}
else{
  $edit_pass =  edit_user_pass($new_pass);
  if($edit_pass['status']=="ok"){
    $result['msg']='Password successfully updated. ';
    $result['log']='pass';
  }else{
      $result['msg']='Could not update password! ERROR: '.$edit_pass['status'];
    $result['log']='fail';
  }
}

echo json_encode($result);