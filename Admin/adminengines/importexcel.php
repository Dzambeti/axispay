
<?php


require_once '../../DB/dbapi.php';


ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once ('SimpleXLSX.php');
		
		//$userid = getuserid($Username);
		
$uploadedStatus = 0;

//select validation
if (isset($_FILES["file"])) {
//if there was an error uploading the file
if ($_FILES["file"]["error"] > 0) {
echo "File Error Code: " . $_FILES["file"]["error"] . "<br/>";
}
else {
  
  /*  
if (file_exists($_FILES["file"]["name"])) {
	
chown($_FILES["file"]["name"],465); //Insert an Invalid UserId to set to Nobody Owner; for instance 465
//$do = unlink($FileName);
unlink($_FILES["file"]["name"]);
}*/
$target = '../bulkimports/';
@$target_file = $target . basename($_FILES["file"]["name"]);

move_uploaded_file($_FILES["file"]["tmp_name"],$target_file);
$uploadedStatus = 1;
}
} 

else 
{
echo "No file selected <br/>";
}


if($uploadedStatus==1)
{

$inputFileName = $target_file; 

try {
  if ( !$xlsx = SimpleXLSX::parse($inputFileName) ) {
            echo SimpleXLSX::parseError();
	} 
}
 catch(Exception $e) {
	die('Error loading file: '.$e->getMessage());
}

$dim = $xlsx->dimension();
$cols = $dim[1];


// delete ratepayers

DeleteBaCustData();

foreach($xlsx->rows() as $k => $r){
    
	if ($k == 0) continue;

	// [0] => ConsumerAccount
    // [1] => ConsumerName
    // [2] => ConsumerAddress
    // [3] => ConsumerPhone
    // [4] => ConsumerEmail
    // [5] => ConsumerIDNum
    // [6] => ConsumerBalance
    // [7] => ConsumerOnlineBalance
    // [8] => CurrentBal

	$Acc = $r[0];
	$Name = $r[1];
	$ConsumerAddr = $r[2];
	$ConsumerPhone = $r[3];
	$ConsumerEmail = $r[4];
	$ConsumerIDNum = $r[5];
	$ConsumerBalance = $r[6];
	$ConsumerOnlineBalance = $r[7];
	$CurrentBal = $r[8];
	$LastSyncBy = 1;
	
	$upload = uploadImport($Acc, $Name, $ConsumerAddr, $ConsumerPhone, $ConsumerEmail, $ConsumerIDNum,$ConsumerBalance,$ConsumerOnlineBalance,$CurrentBal,$LastSyncBy);

	if($upload["status"] == "ok"){
		$excelresponsemsg = '<div class="alert alert-success"> Rate payers have been uploaded successfully </div>';
	}
	else{
		$excelresponsemsg = $upload["status"];
	}
}
echo '<br>'.$excelresponsemsg.'<br>';	
}



?>
