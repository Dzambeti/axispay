<?php

require '../AdminDB/ODBCAdmin.php';
require '../AdminDB/DBAPI.php';

 DelActBtypes();
$BalTypes = getActualBalanceTypes();

foreach($BalTypes as $Bty){
    
    $bmfType = $Bty["bmf-type"];
    $bmfDesc = $Bty["des-eng"]  ;
    $RecCode = $Bty["rcpt-code"] ;
    $SyncBy = $_SESSION["acc"];
 
                  
     $UpldData =  InsertActBalanceTypes($bmfType,$bmfDesc,$RecCode,$SyncBy);
}

if($UpldData["status"]=="ok")
{
    $rslt["status"] = "ok";
}
else{
     $rslt["status"] = "error";
     $rslt["msg"]=$UpldData["status"];
}

echo json_encode($rslt);