<?php

require '../../DB/ODBC.php';

$UserID = $_GET["id"];
$userfirstsec = $_POST["userfirstsec"];
$username = $_POST['usercode'];
$phone_number = $_POST['phone'];


//check email, phone number and  username
$usernames = array();

$all_users = get_all_users_expt_this($UserID);
foreach ($all_users as $user) {
    $DB_username = $user['UserCode'];
    array_push($usernames, $DB_username);
}

if (empty($username) || empty($userfirstsec)) {
    $rslt["msg"] = 'Username or userdetail (first and second names) can not be empty!';
    $rslt["status"] = "error";
}  elseif (in_array($username, $usernames)) {
    $rslt["msg"] = ' Username in use. Please choose another username.';
    $rslt["status"] = "error";
} else {
         $edit_user =   edit_user($username, $userfirstsec, $phone_number, $UserID);
    if ($edit_user['status'] == "ok") {
        $rslt["msg"] = 'User Profile has been edited successfully!';
        $rslt["status"] = "ok";
    } else {
        $rslt["status"] = "error";
        $rslt["msg"] = ' Failed to edit user account. ERROR: ' . $edit_user['status'];
    }
}

echo json_encode($rslt);







