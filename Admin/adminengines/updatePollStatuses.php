<?php

require ('../../library/httpful.phar');
require '../../DB/dbapi.php';
require '../AdminDB/mesgAPI.php';
require '../../Ecocash/vendor/autoload.php';

$AccRecDet = getNonAttempts();

if (count($AccRecDet) > 0) {

    foreach ($AccRecDet as $dt) {


        $pollUrl = $dt["poll_url"];

        if (NetworkConnected() == "yes") {
            $callback = \Httpful\Request::get($pollUrl)->send();   //return array
            $response = ParseMyMsg($callback);
        } else {
            echo "<div class='alert alert-danger'>No internet connection established. Please connect to network services.</div>";
            EXIT;
        }

        $status = $response['status'];
        $attempt_id = $response['reference'];
        updateAttemptStatus($status, $attempt_id);

        if ($status == "Paid") {
            $Poll_attempt_id = $response['reference'];
            $PaynowRef = $response["paynowreference"];
            //update status


            $attempt_det = GetOrderDet($Poll_attempt_id);  // order ID as response from paynow

            $transactionID = substr_replace($Poll_attempt_id, "Rec", 0, 8); //change attempt to transaction ID
            $amount = $response['amount']; //get amount
            $acc_no = $attempt_det[0]['AccountNumber']; //getAccMNum
            $Phone = $attempt_det[0]['phone'];
            $Email = $attempt_det[0]['email'];
            $AccRef = $attempt_det[0]['AccRef'];
            $enc_id = base64_encode($transactionID);
            $att = LogTransID($transactionID, $Poll_attempt_id);

            $phonenum = $Phone;
            $TransSummary = 'Payment of ' . $amount . ' from account number ' . $acc_no . ' has been made successfully.';


            $CreateReciept = CreateExceptReceipt($transactionID, $Poll_attempt_id, $amount,$acc_no,$AccRef, $Phone, $Email, $PaynowRef);

            if ($CreateReciept['status'] == 'ok') {

                //allocate payments
                $Bal = GetBal($acc_no);

                $message = 'Payment of $' . $amount . ' with trans ID ' . $transactionID . ' from account number ' . $acc_no . ' has been made successfully!';
                $la = GetLADetails();

                $LAcode = $la[0]["LaCode"];
                $balance = getBalance();
                $bal = $balance[0]['creds'];
                $credit = ceil(strlen($message) / 160);
                if ($bal >= $credit) {
                    SendSMS($phonenum, $LAcode, $message);
                    reduceToSendCredits($credit);
                }


                // we building raw data ;
              //  $ReturnUrl = "$retUrl/invoice?RefUrl=" . $enc_id;

                $msg = "<div class='alert alert-success'>Exceptions have been processed successfully.</div>";
            } else {
                $msg = $CreateReciept['status'];
                //  echo $msg;
            }
        } else {
            // update status 
          //  $actual_link = "$retUrl/home";
            $msg = "<div class='alert alert-success'>Exceptions have been processed successfully.</div>";
        }
    }
} else {
    $msg = "<div class='alert alert-success'>No Exceptions to process</div>";
}

echo $msg;

function ParseMyMsg($msg) {
    //convert to array data  
    $parts = explode("&", $msg);
    $result = array();
    foreach ($parts as $i => $value) {
        $bits = explode("=", $value, 2);
        $result[$bits[0]] = urldecode($bits[1]);
    }

    return $result;
}

function SendSMS($user_number, $msg_from, $message) {
    if (strpos(trim($msg_from), "+") === 0) {
        $msg_from = substr($msg_from, 1);
    }

    if (strpos(trim($user_number), "0") === 0) {

        $user_number = "263" . substr($user_number, 1);
    } elseif (strpos(trim($user_number), "7") === 0) {

        $user_number = "263" . substr($user_number, 1);
    } else if (strpos(trim($user_number), "+") === 0) {
        $user_number = substr($user_number, 1);
    } elseif (strpos(trim($user_number), "2") === 0) {
        $user_number = $user_number;
    }

    $mymessage = urlencode($message);
    $url = "http://api.bluedotsms.com/api/mt/SendSMS?user=Axis&password=1234567890&senderid=$msg_from&channel=Normal&DCS=0&flashsms=0&number=$user_number&text=$mymessage";
    $response = file_get_contents($url);
    $json = json_decode($response, TRUE); //generate array object from the response from the web
    return $json;
}
