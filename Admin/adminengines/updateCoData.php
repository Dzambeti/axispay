<?php
require '../../DB/ODBC.php';

$LAName = $_POST["laName"];
$LaCode = $_POST["lacode"];
$Address = $_POST["address"];
$VATNum = $_POST["vatNum"];
$CoEmail = $_POST["email"];
$opcode = $_POST["opcode"];
$mcno = $_POST["mcno"];
$intKey = $_POST["intkey"];
$intID = $_POST["intID"];
$retURL = $_POST["retURL"];
$CoDetails = GetLADetails();
$id = $CoDetails[0]["id"];

$UpdateData = UpdateLaDet($LAName,$LaCode,$Address,$VATNum,$CoEmail,$opcode,$mcno,$intKey,$intID,$retURL,$id);

  if($UpdateData['status']=="ok"){
    $result['msg']='<div class="alert alert-success">Company Details Updated Successfully. </div>';
    $result['log']='pass';
  }else{
      $result['msg']='<div class="alert alert-danger>"Could not update details! ERROR: '.$UpdateData['status'].'</div>';
    $result['log']='fail';
  }


echo json_encode($result);