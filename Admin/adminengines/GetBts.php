<?php
require '../AdminDB/ODBCAdmin.php';
require '../../DB/ODBC.php';

 DelBtypes();
$BalTypes = GetBalTypes();
foreach($BalTypes as $Bty){
    
    $IncCode = $Bty["code"];
    $IncomeCodeDesc = $Bty["desc-eng"]  ;
    $IncType = $Bty["inc-type"] ;
    $LedgerAcc = $Bty["alloc"] ;
    $BalanceTypeCode = $Bty["bmf-type"] ;
    $linkToAcc = $Bty["inc-type"] == "C" ? true : false;
     $UpldData =  UploadBalTypes($IncCode,$IncomeCodeDesc,$IncType,$LedgerAcc,$BalanceTypeCode,$linkToAcc);
}

if($UpldData["status"]=="ok")
{
    $rslt["status"] = "ok";
}
else{
     $rslt["status"] = "error";
     $rslt["msg"]=$UpldData["status"];
}

echo json_encode($rslt);