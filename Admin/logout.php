<?php
require_once '../DB/ODBC.php';

if($_SESSION['acc']!="")
{
	redirect("index?acc=".$_SESSION['acc']);
}
if(isset($_GET['logout']) && $_GET['logout']=="true")
{
	logout();
	redirect("index");
}
if(!isset($_SESSION['acc']))
{
    redirect("index");
}
