<?php 
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
 $Users = GetUsers();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title>Axispay | Users</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- wysihtml5 CSS -->
    <link rel="stylesheet" href="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
    <!-- Dropzone css -->
    <link href="../plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
     <link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="../css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/style.min.css" rel="stylesheet">
                              
    <!-- color CSS -->
    <link href="../css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
        <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        
      <?php  require 'header.php'; ?>
        
        <div id="page-wrapper">
            
            
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Rate Payers</h4> </div>
                        
                   
                    <!-- /.col-lg-12 -->
                </div>
                
                <div class="container-fluid newUserdivs">
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3 class="box-title m-b-0">Create New User</h3>

                                <form class="form-horizontal createUser"  method="post">
                                    <div class="form-group">
                                        <label class="col-md-12">Username</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="text" id="usercode" name="usercode" style="border-color: black;" class="form-control" placeholder="Enter username"> </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">First Name</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="text" id="fist" name="userfirst" style="border-color: black;" class="input-group form-control" placeholder="Enter First Name"> </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">Surname</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="text" id="surname" name="usersurname" style="border-color: black;" class="form-control" placeholder="Enter Surname"> </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">Phone</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="tel" id="phone" name="phone" style="border-color: black;" class="form-control" placeholder="Enter phone number"> </div>
                                    </div>
                                    
                                     <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 createuser">Create User</button>
                                    </div>
                                </div>
                                    
                                  <br>    <div class="ajax-loaders saving-spinner slider"> <b>...Creating new user...</b></div>  
                                       <div class="resp col-md-6"></div>  
                    
                                   
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                
                </div>
        <!-- /#page-wrapper -->
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="col-lg-3 col-sm-3 col-xs-3">
                                    <a class="btn btn-block btn-primary btn-rounded newusers">New User</a>
                            </div>
                           <br>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th> User Code #</th>
                                            <th>Username</th>
                                            <th>Phone</th>
                                             <th>Date Created </th>
                                              <th>Actions </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                       
                                        foreach($Users as $rp){
                                         
                                            $UserCode  = $rp["UserCode"]; 
                                             $UserName  = $rp["Username"]; 
                                             $Phone = $rp["Phone"];
                                              $DateCreated   = date("d M y",  strtotime($rp["DateCreated"]));
                                              $id = $rp["id"];
                                             
                                              
                                        ?>
                                        <tr>
                                            <td><?php echo $UserCode; ?></td>
                                            <td><?php echo $UserName; ?></td>
                                            <td><?php echo $Phone; ?></td>
                                            <td><?php echo $DateCreated; ?></td>
                                           <td>
                                               <?php if ($_SESSION['UserCode']  == $UserCode) { ?>
                                               <a href="EditUser.php?uid=<?php echo base64_encode($id); ?>" class="btn" title="edit my profile"><i class="fa fa-edit p-r-10"></i></a>
                                               <?php } else { ?>
                                            <a href="#" class="btn" disabled="true" title="Can not edit this user."><i class="fa fa-edit p-r-10"></i></a>
                                            <?php } ?>
                                           </td>
                                           </tr>
                                        <?php } ?>
                            
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- /.row -->
                
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; AxisPay by Axis Solutions</footer>
        </div>
        
        
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bootstrap/dist/js/tether.min.js"></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
     <script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <script src="../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="../plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
    <script>
        $(document).ready(function () {
            $('.textarea_editor').wysihtml5();
             $('#myTable').DataTable();
             $(".slider").hide();
             $(".newUserdivs").hide();
             
             $(".newusers").click(function(ev){
                 ev.preventDefault();
                 $(".newUserdivs").show("slow");
             });
             
             $(".createuser").click(function(ev){
                 ev.preventDefault();
                 $(this).prop("disabled",true);
                 $(".slider").slideDown(1000);
                 $.post("adminengines/newuser.php",$(".createUser").serialize(),function(resp)
                 {
                    var fdbk = $.parseJSON(resp);
                     if(fdbk.status==="ok")
                     {
                         alert(fdbk.msg);
                          var delay = 3000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                     }
                     else
                     {
                          $(".slider").html(jsnFbk.msg); 
                          $(".slider").slideUp("slow");
                           $(".createuser").prop("disabled",false);
                     } 
                     
                 });
                 
                 
             });
             
          
                 
                 
             });
      
    </script>
    <!-- Custom Theme JavaScript -->
    <script src="../js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>