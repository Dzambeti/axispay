<?php


require_once('../AdminDB/DBAPI.php');
include_once("../libs/messaging.php");
include_once("../libs/InputValidator_class.php");

//load json
// traversing thru jsn elements
//get root message
//check validation
//next message until end:true


class FormControl {

    private $formName;
    private $expiryDate;
    private $creator;
    private $datecreated;
    private $data;
    private $msg_id;

    function __construct($Name) {
        $formData = $this->loadJson($Name);
        $this->expiryDate = $formData['expiry'];
        $this->creator = $formData['creator'];
        $this->datecreated = $formData['date_created'];
        $this->data = json_decode($formData['data'], true);
    }

    function loadJson($formName) {
        return GetJson($formName);
    }

    function isActive() {
        if ($this->expiry > date("d-m-Y H:i:s")) {
            return false;
        } else {
            return true;
        }
    }

    //get function
    function formName() {
        return $this->formName;
    }

    function getExpiryDate() {
        return $this->expiryDate;
    }

    function getCreator() {
        return $this->creator;
    }

    function getData() {
        return $this->data;
    }

    function getExpiry() {
        return $this->expiry;
    }

    //utility mthods
    //check all data elements
    function searchMessages($msgId) {
        $rslt;
        $found = false;
        $response = array();
        foreach ($this->getData() as $AllData) {
            if ((string) $AllData['id'] == $msgId) {
                $rslt = $AllData;
                $found = true;
                $response["done"] = true;
                $response["status"] = "OK";
                $response["found"] = $found;
                $response["userMsg"] = (string) $rslt['msg'];
                $response["msgbdy"] = (string) $rslt['msgbdy'];
                $response["userLvl"] = (string) $rslt['id'];
                $response["lastmsg"] = (string) $rslt['isEnd'];
                $response["nextLvlId"] = (string) $rslt['next'];
                $response["validations"] = (string) $rslt['validations'];
                //return msg within specified level
                break;
            }
        }
        return $response;
    }

    function getRootMessage() {
        $rslt;
        $found = false;
        $response = array();

        foreach ($this->getData() as $AllData) {
            if ((string) $AllData['isRoot'] == "true") {
                $rslt = $AllData;
                $found = true;
                $response["done"] = true;
                $response["status"] = "OK";
                $response["found"] = $found;
                $response["userMsg"] = (string) $rslt['msg'];
                $response["userLvl"] = (string) $rslt['id'];
                $response["validations"] = (string) $rslt['validations'];
                $response["msgbdy"] = (string) $rslt['msgbdy'];
                //$response["userLvl"] =(string)$rslt['id'];
                break;
            }
        }
        return $response;
    }

//validat input
    function validateInput($input, array $validations) { //check if user sent response is of the correct data type
        //$validator = new InputValidator($input);
        $errors = array();
        $valid = true;
        $i = 0;
        foreach ($validations as $validation) {
            if (!$this->getValidationFunction($input, $validation)) {
                $errors[$i] = $this->getValidationErrors($validation);
                $valid = false;
            }
            $i++;
        }

        return $valid ? array("status" => "valid") : array("status" => "invalid", "errors" => $errors);
    }

    function getValidationFunction($input, $validation_string) {
        $validator = new InputValidator($input);

        if ($validation_string == "isString()") {
            return $validator->isString();
        } else if ($validation_string == "isIdentifier()") {
            return $validator->isIdentifier();
        } else if ($validation_string == "isDate()") {
            return $validator->isDate();
        } else if ($validation_string == "isDOB()") {
            return $validator->isDOB();
        } else if ($validation_string == "isNumber()") {
            return $validator->isNumber();
        } else if (preg_match("/\bstartsWith/i", $validation_string)) {
            $arg = substr($validation_string, strpos($validation_string, '"') + 1, -2);
            return $validator->startsWith($arg);
        } else if (preg_match("/\bcontains/i", $validation_string)) {
            $arg = substr($validation_string, strpos($validation_string, '"') + 1, -2);
            return $validator->contains($arg);
        } else if (preg_match("/\bhasNumber/i", $validation_string)) {
            $arg = substr($validation_string, strpos($validation_string, '"') + 1, -2);
            return $validator->hasNumber($arg);
        }
    }

    function getValidationErrors($validation_fxn) {

        if ($validation_fxn == "isString()") {
            return "expected a string";
        } else if ($validation_fxn == "isIdentifier()") {
            return "expected a valid name";
        } else if ($validation_fxn == "isDate()") {
            return "expected a valid date in the format Y-m-d eg 1990-01-12 for 12 January 1990";
        } else if ($validation_fxn == "isDOB()") {
            return "expected a date at least 18 years earlier than today in the format Y-m-d eg 1990-01-12 for 12 January 1990";
        } else if ($validation_fxn == "isNumber()") {
            return "expected a number";
        } else if (preg_match("/\bstartsWith/i", $validation_fxn)) {

            $arg = substr($validation_fxn, strpos($validation_fxn, '"') + 1, -2);
            return "expected a string that starts with $arg";
        } else if (preg_match("/\bcontains/i", $validation_fxn)) {
            $arg = substr($validation_fxn, strpos($validation_fxn, '"') + 1, -2);
            return "expected a string that contains $arg";
        } else if (preg_match("/\bhasNumber/i", $validation_fxn)) {
            $arg = substr($validation_fxn, strpos($validation_fxn, '"') + 1, -2);
            return "expected a string that contains the number $arg";
        }
    }

}

?>