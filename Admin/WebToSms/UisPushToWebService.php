8<?php
include_once("../webToSms/UIShandler.php");
include_once("../libs/messaging.php");
include_once("../webToSms/uisFormSession.php");

@$msg_body = $_POST['message'];
@$msg_from = $_POST["from"];
@$rcvd = date("Y-m-d H:i:s", strtotime($_POST["receive_date"]." ".$_POST['receive_time'])); //
@$sentTime = date("Y-m-d H:i:s", strtotime($_POST["send_date"]." ".$_POST['send_time']));
@$msg_to = "0718436337";

//$msg = new Message(time(),$msg_to, "+263773629282", date("Y-m-d H:i:s"), null, "join");
//handleMsg($msg);

if (isset($msg_body) && isset($msg_from) && isset($rcvd)) {
    $msg = new Message(time(),$msg_to, $msg_from, $sentTime, $rcvd, $msg_body);
   
//$msg->save();
    handleMsg($msg);
    //echo 'result=1';
    //exit();
} else {
    echo "result=0";
    //exit();
}

function generateId() {
    return time();
}

function logMsgToFile(Message $msg) {
    return $msg->getId() . "\t" . $msg->getTo() . "\t" . $msg->getFrom(). "\t" . $msg->getBody() . "\t" . $msg->getReceivedTime() . PHP_EOL;
}


function handleMsg(Message $msg) {
    $number = checkIfNumberExist((string) $msg->getFrom());
    if ($number['status'] == 'failed') {
        harvestContact((string) $msg->getFrom());
    }
    if (checkFormSession($msg->getFrom())) {
        $userDetails = json_decode(getFormSessionDetails($msg->getFrom()), true);

        $formName = $userDetails[0]["formname"];

        $level = $userDetails[0]["Curr_Lvl"]; //id of current level
        // echo 'my level is: '.$level;
        $msg->setFlow(getUisForm($formName)); //LITERALLY SET form name as path 
        $msg->setFlowLevel($level);
        // $msg->updateForm(); //Tag flow before reply
        proceedWithform($level, $msg);
        // echo 'proceeding with flow<br>';
    } else {
       
        check_keyword($msg); //LOCALHFUNC
        
    }
}

function proceedWithform($lvl, Message $msg) { //if this user already has a session
    global $msg;

    $userDetails = json_decode(getFormSessionDetails($msg->getFrom()), true);

    $formName = $userDetails[0]["formname"];
    $UIS = new FormControl($formName);
    $uisMsg = $UIS->searchMessages($lvl); //search current level data


    $validations = explode(",", (string) $uisMsg["validations"]); //array validations
    $validateInput = $UIS->validateInput($msg->getBody(), $validations);

    if ($validateInput["status"] == "valid") {
        saveUISFormData($msg->getFrom(), $uisMsg["userMsg"], $msg->getBody());

        $UisSess = createFormSessionFromDb($msg->getFrom()); // UPDATE session data based on the current level

        if ($uisMsg["lastmsg"] == "true") {
            //kill session
            $uservals = getUserVal($msg->getFrom());

            $name = $uservals[0]['AccName'];
            $LaInf = LADt();
            $LaNm = $LaInf[0]["LAName"];
            $UisSess->delSession();
            $finaltxt = "Thank you  $name for subscribing with $LaNm, we will keep updating you on all matters arising from the LA.";
            $msg->setBody($finaltxt);
            $msg->SendAndSaveMSG();
            
        } 
        elseif ($uisMsg["lastmsg"] == "false") 
            {
            $nextid = $uisMsg["nextLvlId"];
            $newLvl = $UIS->searchMessages($nextid);
            $msg->setBody($newLvl["msgbdy"]);
            $msg->setFlow(getUisForm($formName));
            $msg->setFlowLevel($newLvl["userLvl"]);
            //$msg->save(); //save msg and update db setting
            $UisSess->updateSessionLevel($newLvl["userLvl"]); //update session level
            $msg->SendAndSaveMSG();
            
        }
    } else {
        $txt = "Input Error : " . implode(PHP_EOL, $validateInput["errors"]) . ". Please correct the error and resend your input";
        $msg->setBody($txt);
        $msg->SendAndSaveMSG();
    }
}

function check_session(Message $msg) {

    if (checkFormSession($msg->getFrom())) {
        return true;
    } else {
        return false;
    }
}

function check_keyword(Message $msg) {
    $chek_result = json_decode(verify_triggerUIS($msg->getKeyword()), true);
 
    
    if ($chek_result["status"] == true) {
        //echo 'checking keyword';

        $msg->setValidity(true);
        @$uis = new FormControl($chek_result["name"]); //name of the frm

        $ses = new UisSession($msg->getFrom(), $chek_result["name"]);
        $sessroot = $uis->getRootMessage();

        $ses->updateLevel($sessroot["userLvl"]); //create session
        $ses->setInSession();

         $ses->save();
        
        $msg->setBody($sessroot["msgbdy"]);
        
       $resp = $msg->SendAndSaveMSG();
       
      
    } else { //keyword not found ignore->message is a lost one
        $resp = "Take it here.";
        $file = fopen("../logs/msgres.txt", "a");
     fwrite($file, json_encode($resp));
    fclose($file);
    }
}

function replyErrorMessage(Message $msg) {
    $response = "Message could not fit any of the categories we know. You may check your message and attempt resending";
    $msg->setBody($response);
    $msg->SendAndSaveMSG();
//print_r($msg);
}

function replyInvalidResponse(Message $msg) {
    $response = "Invalid data received as an answer to the previous question. Please try again";
    $msg->setBody($response);
    $msg->SendAndSaveMSG();
}

function isFlowTarget($contact, $flowgroups) {
    $found = false;
    $groups = json_decode($flowgroups, true);
    foreach ($groups as $group) {
        if (isMemberOf($group, $contact)) {

            $found = true;
            return $found;
        }
    }
    return $found;
}

function replyFlowMessages(Message $reply, $groups) {

    //remove keyword from Message
    $full_msg_st = trim($reply->getBody());
    $main = substr($full_msg_st, strpos($full_msg_st, " ") + 1);

    //remove Action Directive
    $toSend = substr(trim($main), strpos(trim($main), " ") + 1);

    //get Authority Details
    $authDtls = getUserDetails($reply->getFrom());
    $from = $authDtls["title"];
    //prepare message
    $authMsg = new Message(time(), "grup", $from, date("Y-m-d H:i:s"), null, $toSend);
    //get flow groups
    $flowgrps = json_decode($groups, true);
    
    foreach ($flowgrps as $flowgrp) {
        $authMsg->setTo($flowgrp);
        $authMsg->sendBulk();
        //print_r($authMsg);	

        echo"<br><br>**************************************END*****************************";
        echo"<br><br>";
    }
    //fo each group
    //send a bulk msg as a reply
}


