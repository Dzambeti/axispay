<?php
require_once('../AdminDB/DBAPI.php');

class UisSession
{
	var $userPhone;
	var $id;
	var $formName;
	 var $start_tym;
	 var $curr_lvl;
	 var $time_done;
	 var $inSession =false; 
	 
	 
	 function __construct($userphone,$formName)
	 {
		 $this->userPhone =$userphone;
		 $this->formName=$formName;
		 $this->start_tym =date("Y-m-d H:i:s");
		 $this->id=time();
		 $inSession=false;
                 $this->time_done = date("Y-m-d H:i:s");
	 }
	 
	 function getCurrLevel()
	 {
		 return $this->curr_lvl;
	 }
	 
	 function updateLevel($lvl)
	 {
		 $this->curr_lvl =$lvl;
		 //$this->update();
	 }
	 
         
         function updateSessionLevel($lvl)
	 {
		 $this->curr_lvl =$lvl;
		 $this->update();
	 }
	 function setInSession()
	 {
		 $this->inSession=true;
		 //$this->update();
	 }
	  function delSession()
	  {
		   $this->time_done= date("Y-m-d H:i:s");
		  $this->inSession=false;
		   $this->deleteSess();
	  }
	  function save()
	  {
		 $value = createFormSession($this);
                 return $value;
		
	  }
	  
	   function update()
	   {
		    updateFormSession($this);
	   }
	   
	   function deleteSess()
	   {
                delFormSession($this);
	   }
	 function getId()
	 { return $this->id;
	 }
	  
	  function getInSession()
	  {
		  return $this->inSession;
	  }
	  function getPhone()
	  {
		  return $this->userPhone;
	  }
	  function getFormName()
	  {
		  return $this->formName;
	  }
	  function getStartTym()
	  {
		  return $this->start_tym;
	  }
	  function getCurrLvl()
	  {
		  return $this->curr_lvl;
	  }
	  function getTimeDone()
	  {
		   return $this->time_done = date("Y-m-d H:i:s");
	  }
	   
	   function setId($id)
	   {
		   $this->id=$id;
	   }
	   function setStartTym($tym){
		   $this->start_tym=$tym;
	   }
}
?>