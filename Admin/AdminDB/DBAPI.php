<?php
@session_start();
$db = new PDO('mysql:host=localhost;dbname=laaxpay;charset=utf8', 'root', '');
//$db = new PDO('mysql:host=MYSQL5006.site4now.net;dbname=db_a33c8a_axispay;charset=utf8', 'a33c8a_axispay', 'Axis1991');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

function get_month_sales() {
    global $db;
    //$result=array();
    try {
        $this_month = date("m");
        $sql = $db->prepare('select sum(AmountPaid) from reciepts where month(DateCreated) =? and AmountPaid>0');
        $sql->execute(array($this_month));
        $result = $sql->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function get_week_sales() {
    global $db;
    $this_sunday = date("Y-m-d H:i:s", strtotime('last sunday', strtotime('this week', time())));
    $this_sart = date("Y-m-d H:i:s", strtotime($this_sunday . "+6 days"));
    try {
        $sql = $db->prepare('select sum(AmountPaid) from reciepts where DateCreated>=? and DateCreated<=? and AmountPaid>0');
        $sql->execute(array($this_sunday, $this_sart));
        $result = $sql->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function get_year_sales() {
    global $db;
    //$result=array();
    try {
        $this_month = date("Y");
        $sql = $db->prepare('select sum(AmountPaid) from reciepts where year(DateCreated) =? and AmountPaid>0');
        $sql->execute(array($this_month));
        $result = $sql->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function getPayers() {
    global $db;
    //$result=array();
    try {

        $sql = $db->prepare('select count(*) from customerdata');
        $sql->execute();
        $result = $sql->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function SalesByBtype() {
    global $db;
    //$result=array();
    try {
        $this_month = date("Y");
        $sql = $db->prepare('select sum(AmntPaidFor) as Totalu,BalType from settlementsmaster where year(DateCreated) =? and AmntPaidFor>0 group by BalType');
        $sql->execute(array($this_month));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function SalesByAcc() {
    global $db;
    //$result=array();
    try {
        $this_month = date("Y");
        $sql = $db->prepare('select sum(AmountPaid) as Totalu,AccountNumber from reciepts where year(DateCreated) =? and AmountPaid>0 group by AccountNumber');
        $sql->execute(array($this_month));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function MOMSales(){
  global $db;
    //$result=array();
    try {
        //$this_month = date("Y");
        $sql = $db->prepare("SELECT DATE_FORMAT(DateCreated, '%M') AS Month, SUM(AmountPaid) as Tot FROM reciepts GROUP BY DATE_FORMAT(DateCreated, '%M')");
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;   
}




function GetDebts() {
    global $db;
    //$result=array();
    try {

        $sql = $db->prepare('select  sum(ConsumerBalance) as ConsBla,ConsumerAccount from customerdata where ConsumerBalance>0  group by ConsumerAccount  order by ConsBla desc limit 10');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetAllDebts() {
    global $db;
    //$result=array();
    try {

        $sql = $db->prepare('select  sum(ConsumerBalance) as ConsBla,ConsumerAccount from customerdata where ConsumerBalance>0  group by ConsumerAccount');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function TopTenBalances(){ //
   global $db;
    //$result=array();
    try {

        $sql = $db->prepare('select * from customerdata order by ConsumerBalance desc limit 10');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result; 
}

function GetDebtByBtype() {
    global $db;
    //$result=array();
    try {

        $sql = $db->prepare('select sum(AmountDue) as ConsBla,BalType from consumerbalances where AmountDue>0  group by BalType');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetUsers() {
    global $db;
    //$result=array();
    try {

        $sql = $db->prepare('select * from admins');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function showMessages() {
    global $db;

    try {
        $sql = "select * from message";
        $query = $db->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $results = $ex->getMessage();
    }
    return $results;
}

/* * *******************************INTERACTIONS *************************** */

function GetJson($name) {
    global $db;

    try {
        $stm = $db->prepare('select * from form where name = ? ');
        $stm->execute(array($name));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        if ($stm->rowCount() > 0) {
            $response['status'] = 'deleted';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
// echo json_encode($response);
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
        $response['status'] = $ex->getMessage();
    }
    return $result[0];
}

function harvestContact($cell){
   global $db;
   
    try {
        $stm = $db->prepare('insert into uis(CellNum) values(?)');
        $stm->execute(array($cell));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'inserted';
            echo json_encode($response);
        } else {
            $response['status'] = 'failed';
            return json_encode($response);
        }
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    } 
}

function checkIfNumberExist($phone) {
    global $db;
 
    try {
        $stm = $db->prepare('select * from uis where CellNum = ? ');
        $stm->execute(array($phone));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        if ($stm->rowCount() > 0) {
            $response['status'] = 'ok';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
// echo json_encode($response);
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
        $response['status'] = $ex->getMessage();
    }
    return $response;
}

//sessions for the form
function createFormSession(UisSession $uis) {

    global $db;
   
    try {
        $stm = $db->prepare('insert into uisflowsession(phone,formname,Start_tym,Curr_Lvl,Insession,Time_Done,Extlid)  values(?,?,?,?,?,?,?)');
        $stm->execute(array($uis->getPhone(), $uis->getFormName(), $uis->getStartTym(), $uis->getCurrLvl(), $uis->getInSession(), $uis->getTimeDone(), $uis->getId()));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'inserted';
            return json_encode($response);
        } else {
            $response['status'] = 'failed';
            return json_encode($response);
        }
    } catch (PDOExcepion $ex) {
        return $ex->getMessage();
    }
}

function updateFormSession(UisSession $uis) {

    global $db;
    $response;
    try {
        $stm = $db->prepare('update uisflowsession set phone =?, formname=?,Start_tym=?, Curr_Lvl =?,Insession=?,  Time_Done=? where Extlid=?');
        $stm->execute(array($uis->getPhone(), $uis->getFormName(), $uis->getStartTym(), $uis->getCurrLvl(), $uis->getInSession(), $uis->getTimeDone(), $uis->getId()));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'OK';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
        }
        return json_encode($response);
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}

function delFormSession(UisSession $uis) {
    global $db;
    $response;
    try {
        $stm = $db->prepare('delete from uisflowsession where Extlid=?');
        $stm->execute(array($uis->getId()));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'OK';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
        }
        return json_encode($response);
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}

function checkFormSession($phone) {
    global $db;
    
    try {

        $sentSql = "SELECT * FROM (select * from `uisflowsession` where (unix_timestamp() - unix_timestamp(`start_Tym`)) / (60*60)<40)logged_sessions where phone = ?";
        $sentQuery = $db->prepare($sentSql);
        $sentQuery->execute(array($phone));
        $phones = $sentQuery->fetchAll(PDO::FETCH_ASSOC);
        if ($sentQuery->rowCount() > 0) {
            $found = true;
        } else {
            $found = false;
        }
        return $found;
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

//print_r(checkFormSession("+263773629282"));

function getFornSessionDetails($phone) {

    global $db;
    $found;
    $rslt = array();
    $sentSql = "SELECT Curr_Lvl,formname from uisflowsession where phone = ?";
    $sentQuery = $db->prepare($sentSql);
    $sentQuery->execute(array($phone));
    $details = $sentQuery->fetchAll(PDO::FETCH_ASSOC);

    if ($sentQuery->rowCount() > 0) {
        $rslt = json_encode($details);
    }

    return $rslt;
}

function createFormSessionFromDb($phone) {
    global $db;
  
    try {
        $sentSql = "SELECT * from uisflowsession where phone = ?";
        $sentQuery = $db->prepare($sentSql);
        $sentQuery->execute(array($phone));
        $details = $sentQuery->fetchAll(PDO::FETCH_ASSOC);

        if ($sentQuery->rowCount() > 0) {
            $uisSess = new UisSession($phone, $details[0]["formname"]);
            $uisSess->setInSession($details[0]["Insession"]);
            $uisSess->updateLevel($details[0]["Curr_Lvl"]);
            $uisSess->setId($details[0]["Extlid"]);
            $uisSess->setStartTym($details[0]["Start_tym"]);
            // $rslt = json_encode($details[0]);
            $rslt = $uisSess;
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }

    return $rslt;
}

function verify_triggerUIS($keyword) {

    global $db;
    $response;
    try {
        $stm = $db->prepare('select id,name from form where id=(select ref from keyword where keyword=?)');
        $stm->execute(array($keyword));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        if ($stm->rowCount() > 0) {
            $response['status'] = true;
            $response['name'] = $result[0]["name"];
            $response['id'] = $result[0]["id"];
//return  json_encode($response);
        } else if ($stm->rowCount() == 0) {
            $response = array("path" => NULL, "status" => false, "id" => NULL);
        }
        return json_encode($response);
    } catch (PDOExcepion $ex) {
        return $ex->getMessage();
    }
}

function getFormGroups($name) {
    global $db;
    $sql = "SELECT `groups` FROM `form` WHERE `name`=?";
    $query = $db->prepare($sql);
    $query->execute(array($name));
    $results = $query->fetchColumn(0);
    $count = $query->rowCount();
//echo $count;
    return $results;
}

function getFormSessionDetails($phone) {

    global $db;
 
    $rslt = array();
    $sentSql = "SELECT Curr_Lvl,formname from uisflowsession where phone = ?";
    $sentQuery = $db->prepare($sentSql);
    $sentQuery->execute(array($phone));
    $details = $sentQuery->fetchAll(PDO::FETCH_ASSOC);

    if ($sentQuery->rowCount() > 0) {
        $rslt = json_encode($details);
    }

    return $rslt;
}

function updateFormMsg(Message $m) {
    global $db;
    $response;
    try {
        $stm = $db->prepare('update message set flow =?, flow_level=?, isValid=? where extId=?');
        $stm->execute(array($m->getFlow(), $m->getFlowLevel(), $m->isValid(), $m->getId()));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'OK';
            echo json_encode($response);
        } else {
            $response['status'] = 'failed';
// echo json_encode($response);
        }
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}



function saveRplyFormMsg(Message $m, $credit) {
    global $db;

    try {
        $stm = $db->prepare('insert into message(msg_from,msg_to,body,sentTime,receivedTime,status,user,Total_sms) values(?,?,?,?,?,?,?,?)');
        $stm->execute(array($m->getTo(), $m->getFrom(), $m->getBody(), $m->getSenttime(), $m->getReceivedtime(), $m->getStatus(), $m->getUser(), $credit));
        if ($stm->rowCount() > 0) {
            $lastId = $db->lastInsertId();
            $response['status'] = 'Saved';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
        }
        return $lastId;
    } catch (PDOExcepion $ex) {
        return $ex->getMessage();
    }
}

function getUisForm($name) {
    global $db;
    $sql = "SELECT `id` from `form` WHERE `name`=?";
    $query = $db->prepare($sql);
    $query->execute(array($name));
    $results = $query->fetchColumn(0);
    $count = $query->rowCount();
    echo $count;
    return $results;
}

function getAllForms() {
    global $db;
    $sql = "SELECT * from form";
    $query = $db->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);
    $count = $query->rowCount();
    echo $count;
    return $results;
}

/* save session flow data inputs as users submit their responses to messages begging input */

function saveUISFormData($phone, $field, $value) 
        {
    global $db;
    
    try {
        $nufield = trim($field);
        $stm = $db->prepare("update uis set $nufield = ? where CellNum=?");
        $stm->execute(array($value, $phone));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'OK';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
        }
        return json_encode($response);
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}



function createUisForm($FormName, $data, $expiry_Date, $Keyword) {
 global $db;
    try {
        $db->beginTransaction();
        $stm = $db->prepare("insert into form(name,creator,data,date_created,expiry) values(?,?,?,?,?)");
        $stm->execute(array($FormName, $_SESSION["acc"], $data, date("Y-m-d H:i:s"), $expiry_Date));
        $getLastId = $db->lastInsertId();
  foreach ($Keyword as $trigger) {
      $typ = "UIS";
            $stm = $db->prepare('insert into keyword(keyword,ref,type) values(?,?,?)');
            $query = $stm->execute(array($trigger, $getLastId,$typ));
        }
        if ($stm->rowCount() > 0) {
            $response['status'] = 'ok';
            $db->commit();
        } else {
            $response['status'] = 'failed';
            $db->rollBack();
        }
       
    } catch (PDOException $ex) {
        $response = $ex->getMessage();
    }
     return $response;
}

function getUISKeywords() {
    global $db;
    $sentSql = "SELECT keyword FROM keyword where ? ORDER BY KEYWORD ASC ";
    $sentQuery = $db->prepare($sentSql);
    $sentQuery->execute(array(1));
    $keywords = $sentQuery->fetchAll(PDO::FETCH_ASSOC);
    return $keywords;
}

function GetFormKeywords($formref){
     global $db;
    $sentSql = "SELECT keyword FROM keyword where ref = ?";
    $sentQuery = $db->prepare($sentSql);
    $sentQuery->execute(array($formref));
    $keywords = $sentQuery->fetchAll(PDO::FETCH_ASSOC);
    return $keywords;
}

function getUserVal($phone) {
    global $db;
    $sentSql = "SELECT * from uis where CellNum=? order by id desc";
    $sentQuery = $db->prepare($sentSql);
    $sentQuery->execute(array($phone));
    $details = $sentQuery->fetchAll(PDO::FETCH_ASSOC);
    return ($details);
}

function getNumOfParticipants($Flowref) {
    global $db;
    $sentSql = "SELECT count(*) as freq from uisflowsession where formname=?";
    $sentQuery = $db->prepare($sentSql);
    $sentQuery->execute(array($Flowref));
    $details = $sentQuery->fetchColumn();
    return ($details);
}


// admin function should all come here
function MyPassky(){
 
    global $db;
    try {

        $sql = $db->prepare('select * from admins where id=?');
        $sql->execute(array($_SESSION['acc']));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;

}

function edit_user_pass($passkey) {
    global $db;
    $result = array();
    try {
        $sql = $db->prepare('update admins set Password=? where id=?');
        $sql->execute(array($passkey,$_SESSION['acc']));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function ShowBalanceTypes(){
    global $db;
    try {

        $sql = $db->prepare('select * from balancetypemaster');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
          } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
   
}

function InsertActBalanceTypes($bmfType,$bmfDesc,$RecCode,$SyncBy){
  global $db;
 
    try{
        $stm =  $db->prepare("insert into  ActualBalanceTypes(bmfType,bmfDesc,RecCode,SyncBy) values(?,?,?,?)");
        $stm->execute(array($bmfType,$bmfDesc,$RecCode,$SyncBy));
       $count = $stm->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
        
    } catch (Exception $ex) {
               $result["status"]=$ex->getMessage();
    }
    
    return $result;   
}


function ShowActBalanceTypes(){
    global $db;
    try {

        $sql = $db->prepare('select * from ActualBalanceTypes');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
          } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
   
}

function DelActBtypes(){
     global $db;
    try {
        
        $sql = $db->prepare('delete from actualbalancetypes');
        $sql_alt = $db->prepare('ALTER TABLE actualbalancetypes AUTO_INCREMENT=1'); 
        $sql->execute();
         $sql_alt->execute();
       $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
      
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}




