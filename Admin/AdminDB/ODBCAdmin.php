<?php


try{
   $conn = new PDO ('odbc:inc', 'Sysprogress', 'Sysprogress');
$recODBC = new PDO ('odbc:rec', 'Sysprogress', 'Sysprogress');
$ProODB = new PDO ('odbc:promun', 'Sysprogress', 'Sysprogress'); 
} catch (Exception $ex){
    echo "   <br><b>No connection to Database!!!!!!! <br><br>Error - </b><em style='color:red'>".$ex->getMessage();
    die();
}



function SyncReceipts($Acc,$Amnt,$IncCode,$AccName,$VatAmt)
        {
    global $conn;
    try {
        $ProSQL = "insert into PUB.munrct(acc amount code component eff-date eff-datetime line mcno opcode paytype rec-date rec-name rec-status rec-time seq-no vat-amt) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $Sql = $conn->prepare($ProSQL);
        $Sql->execute(array($Acc,$Amnt,$IncCode,"no",date("d/m/y"),date("d/m/Y H:i:s"),1,001,"1","C",date("d/m/y"),$AccName,"U",date("H:i"),1,$VatAmt));
        $Counter = $Sql->rowCount();
        if($Counter>0)
        {
            $rslt["status"] = "ok";
        }
        else{
            $rslt["status"] = "error";
        }
        } catch (Exception $ex) {
        $rslt = $ex->getMessage();
    }

    return $rslt;
}


//company details
function GetCoDetails() {
    global $ProODB;
    try {
        $sql = $ProODB->prepare('SELECT * FROM PUB.procof where company=0');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function GetUserData() { //test this function
    global $ProODB;
    try {
        $ProSQL = "SELECT * FROM PUB.pass-file";
        $Resulst = odbc_exec($ProODB,$ProSQL);
        while ($myRow = odbc_fetch_array($Resulst)) {
            $rows[] = $myRow;
        }
    } catch (Exception $ex) {
        $rows = $ex->getMessage();
    }
        return $rows;
}

function get_all_debtors() 
{
    global $conn;
    try {
        $sql = $conn->prepare('SELECT PUB.muncmf.acc,PUB.muncmf.name,PUB.muncmf.addr1,PUB.muncmf.addr2,PUB.muncmf.addr3,PUB.muncmf.cell,PUB.muncmf.phone,PUB.muncmf.email,PUB.muncmf.idno,PUB.muncmf.balance FROM PUB.muncmf where company=0 and active != 99');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

//print_r(get_all_debtors());


function GetStatement() 
{
    global $conn;
    try {
        $sql = $conn->prepare(' select * from PUB.muntdf');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}


function GetBalTypes() 
{
    global $recODBC;
    try {
        $sql = $recODBC->prepare("select * from PUB.municf");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

//print_r(GetBalTypes());



function GetCustomerBalances($Acc) 
{
     global $conn;
    try {
        $sql = $conn->prepare(' select * from PUB.munbmf where acc = ?');
        $sql->execute(array($Acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) 
    {
        $result = $ex->getMessage();
    }
    return $result; 
}
//print_r(GetCustomerBalances(1));




function all_receipts() {
    global $recODBC;
    try {
        $recsql = "SELECT * FROM PUB.munrct";
        $recres = odbc_exec($recODBC, $recsql);

        while ($myRow = odbc_fetch_array($recres)) {
            $rows[] = $myRow;
        }
    } catch (Exception $ex) {
        $rows = $ex->getMessage();
    }

    return $rows;
}

function CreateMunrctctr($mcno, $cash, $recstatus, $recdate) {
    global $recODBC;
    try {
        $valid = 0;
        $sql = $recODBC->prepare('insert into PUB.munrctctr(mcno,cash,valid,"rec-status","rec-date") values (?,?,?,?,?)');
        $sql->execute(array($mcno, $cash, $valid, $recstatus, $recdate));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function CreateMunrct($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname) {
    global $recODBC;
    try {

        $sql = $recODBC->prepare('insert into PUB.munrct(code,acc,amount,mcno,"rec-date",recno,opcode,ref,paytype,"seq-no","rec-status","rec-name") values (?,?,?,?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function getActualBalanceTypes(){
   global $conn;
    try {
        $sql = $conn->prepare('SELECT "bmf-type","des-eng","rcpt-code" from  PUB.muntyp');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function GetTotalUnprocessed($acc)
{
     global $recODBC;
    try {
        $var = '"rec-status"';
        $sql = $recODBC->prepare("SELECT sum(PUB.munrct.amount)  as tot from PUB.munrct where  acc = ? and $var <> 'P'");
        $sql->execute(array($acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

Function getAccTransHis($acc,$startDate, $endDate){
 global $conn;
    try {
        $sql = $conn->prepare('SELECT acc,amt,period,prog,ref , "bmf-type", "vat-amt" , "tr-date" from  PUB.munthf 
                where  acc = ? and "tr-date" >= ? and "tr-date" <= ? ');
        $sql->execute(array($acc,$startDate, $endDate)); 
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}







