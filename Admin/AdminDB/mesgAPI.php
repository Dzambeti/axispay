<?php

@session_start();
@ob_start();
define('ROOT_DIR', dirname(__FILE__));

$db = new PDO('mysql:host=localhost;dbname=laaxpay;charset=utf8', 'root', '');
//$db = new PDO('mysql:host=MYSQL5006.site4now.net;dbname=db_a33c8a_axispay;charset=utf8', 'a33c8a_axispay', 'Axis1991');

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

function contacts($startacc,$endacc) {
    global $db;
    //$result=array();
    try {
        $num = "";

        $sql = $db->prepare('select * from customerdata where ConsumerAccount between ? and ? and ConsumerPhone != ?  AND ConsumerPhone is not NULL ');
        $sql->execute(array($startacc,$endacc,$num));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function LADt() { //lamaster
    global $db;
    try {
        $sql = $db->prepare('select * from lamaster');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function saveToSendMsg(Message $m, $credit) {
    global $db;

    try {
        $stm = $db->prepare('insert into message(msg_from,msg_to,body,sentTime,receivedTime,status,user,Total_sms) values(?,?,?,?,?,?,?,?)');
        $stm->execute(array($m->getFrom(), $m->getTo(), $m->getBody(), $m->getSenttime(), $m->getReceivedtime(), $m->getStatus(), $m->getUser(), $credit));
        if ($stm->rowCount() > 0) {
            $lastId = $db->lastInsertId();
            $response['status'] = 'Saved';
            //echo json_encode($response);
        } else {
            $response['status'] = 'failed';
        }
        return $lastId;
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}

function addToSendGroupSms($loopCredit, $lstinsertid) {
    global $db;
    $rslt;
    try {
        $sql = "update groupmessages  set Total_sms=Total_sms+? where account=? and id=? and flag=1";
        $custquery = $db->prepare($sql);
        $custquery->execute(array($loopCredit, $_SESSION['acc'], $lstinsertid));
        $custcount = $custquery->rowCount();
        if ($custcount > 0) {
            $rslt["status"] = true;
            $rslt["msg"] = "done";
        } else {
            $rslt["status"] = false;
            $rslt["msg"] = "failed";
        }
        return $rslt;
    } catch (Exception $ex) {
        echo $ex->message();
    }
}

function reduceToSendCredits($msgcredits) {

    global $db;
    try {
        $sql = "update lamaster set SMSCreds=SMSCreds-?";
        $custquery = $db->prepare($sql);
        $custquery->execute(array($msgcredits));
        $custcount = $custquery->rowCount();
        if ($custcount > 0) {
            $rslt["status"] = true;
            $rslt["msg"] = "done";
        } else {
            $rslt["status"] = false;
            $rslt["msg"] = "failed";
        }
        return $rslt;
    } catch (Exception $ex) {
        echo $ex->message();
    }
}

function getBalance() {
    global $db;

    $sql2 = "SELECT SMSCreds as creds FROM lamaster";
    $credit = $db->prepare($sql2);
    $credit->execute();
    $rslts = $credit->fetchAll(PDO::FETCH_ASSOC);
    return $rslts;
}

function getToSendGroupNumbers($groupName) {
    global $db;
    $i = 0;


//table for groups using district as dummy for now 
    $sql = "select phone from contacts where status='active' and acc=? and id in (select id from usergroup where group_id=(SELECT group_id FROM groups where name=? and flag=1))";
    $grpquery = $db->prepare($sql);
    $grpquery->execute(array($_SESSION['acc'], $groupName));
    $groupresults = $grpquery->fetchAll(PDO::FETCH_ASSOC);
    $counter = $grpquery->rowCount();

    return $groupresults;
}

function getToSendGroupId($name) {

    global $db;

    try {
        $sql = "select group_id from groups where name = ? and acc =?";
        $query = $db->prepare($sql);
        $query->execute(array($name, $_SESSION['acc']));
        $results = $query->fetchColumn();
        if ($query->rowCount() > 0) {
            $rslt = $results;
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
    return $rslt;
}

function saveToSendGroupMessage(Message $m) {
    global $db;
    $response;
    $lastgrpID;
    try {
        $stm = $db->prepare('insert into groupmessages(msg_from,msg_to,body,sentTime,flag,extId,account) values(?,?,?,?,?,?,?)');
        $stm->execute(array($m->getFrom(), $m->getTo(), $m->getBody(), $m->getSenttime(), 1, $m->getId(), $m->getAcc()));
        if ($stm->rowCount() > 0) {
            $lastgrpID = $db->lastInsertId();
            $response['status'] = 'Saved';
// echo json_encode($response);
        } else {
            $lastgrpID = "";
            $response['status'] = 'failed';
            //echo json_encode($response);
        }
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
    return $lastgrpID;
}

function updateToSendMSG($msgid, $lastInsertId) {
    global $db;

    try {
        $stm = $db->prepare('update message set urlMsgId=? where id=?');
        $stm->execute(array($msgid, $lastInsertId));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'OK';
        } else {
            $response['status'] = 'failed';
// echo json_encode($response);
        }
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}



