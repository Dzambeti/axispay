<?php
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Send SMS </title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php require 'header.php'; ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Send SMS</h4> </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                            <div class="white-box">
                                <h5 class=" m-b-0">NB: @name - account name, @bal - account balance, @current - current balance, @acc - acount number, @la - Local Authority Name.</h5>
                                <br><br>
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-12">Sender ID</label>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <?php $getLaData = GetLADetails();
                                            $LaNm = $getLaData[0]["LaCode"];
                                            ?>
                                            <input type="text"  style="border-color: black;" maxlength="10" id="senderID" class="form-control" value="<?php echo $LaNm; ?>"> </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        
                                        <label class="col-md-12">Additional Message:  <span id="xtercount" style="color:#082E87; font-size:16px;">  </span>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      <span  for="credit" style="font-size:16px;" >Credits		:		 </span>  <span id="creditscount" style="color:red; font-size:16px;">  </span>
                                        </label>
                                        <div class="col-md-6 ">
                                            <textarea class="form-control"  style="border-color: black;" id="msgbody"  rows="5"></textarea>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">Accounts Range</label>
                                        <div class="col-md-3 col-sm-3 col-lg-3">
                                         
                                            <input type="text"  style="border-color: black;" id="start_acc" class="form-control" value="0" placeholder="Start Account"> </div>
                                    <div class="col-md-3 col-sm-3 col-lg-3">
                                         
                                        <input type="text"  style="border-color: black;" id="end_acc" class="form-control" value="999999999999" placeholder="End Account"> </div>
                                                                    
                                    </div>
                                    
                                     <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 sendMsg">Send Message</button>
                                    </div>
                                </div>
                                    
                                  <br>    
                                  <div class="form-group m-b-0">
                                      <div class="ajax-loaders saving-spinner"> <b>...Sending Direct Message...</b></div>  
                                       <div class="resp col-md-6"></div> 
                                  </div>
                    
                                   
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                
                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; Axis Solutions </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <script src="js/jasny-bootstrap.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>
            $(document).ready(function(){
             
               	
                                 
$('textarea').on('keyup', function() {
    var preset = 0;
	var chars_count =($(this).val().length) + preset;
	var sms_count =Math.ceil(chars_count/160) ;
	var extra = chars_count%160;
	var remaining = 160-extra;
    
	  $("#xtercount").html(sms_count+"/"+remaining); 
	   $("#creditscount").html(sms_count);  
	 $("#actualcred") .val(sms_count);
  });
 
 
 $(".saving-spinner").hide();
 $(".resp").hide();
		 $('.sendMsg').click(function(){
			 var varInstanrtChcekrstatuer = '<?php echo $_SESSION["acc"]; ?>';
	 $('.sendMsg').prop('disabled', true);
	 $('.saving-spinner').slideDown('slow');
			$.post('adminengines/usersms.php?midastouchvalssientors='+varInstanrtChcekrstatuer,
			{
				senderId: $("#senderID").val(),
				MsgBdy: $("#msgbody").val(),
				creditval: $("#actualcred").val(),
                                startacc: $("#start_acc").val(),
                                endacc: $("#end_acc").val()
			},
			   function(response) {  
                            console.log(response);
                              $('.resp').html(response);
			    $('.saving-spinner').slideUp('slow');
                            $(".resp").show("slow");
                            $('.sendMsg').prop('disabled', false);
				});
		return false;
		});
	
            });
            </script>
    </body>

</html>