<?php
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Update Password </title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php require 'header.php'; ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Change Password</h4> </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3 class="box-title m-b-0">Change Password</h3>

                               <form class="form-horizontal frmUpdatePasskey"  method="post">
                                    <div class="form-group">
                                        <label class="col-md-12">Old Password</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="password" id="oldpasskey" name="oldpass" style="border-color: black;" class="form-control" placeholder="Enter Old Password"> </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">New Password</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="password" id="newpass" readonly="true" name="newpass" style="border-color: black;" class="input-group form-control" placeholder="Enter New Password"> </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">Confirm Password</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="password" id="confirmpass" readonly="true" name="confirmpass" style="border-color: black;" class="form-control" placeholder="Confirm New Password"> </div>
                                    </div>
                                   
                                    <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 btnupdatepasskey">Update Password</button>
                                    </div>
                                </div>
                                    
                                  <br>    <div class="ajax-loaders saving-spinner slider"> <b>...Updating passkey...</b></div>  
                                       <div class="resp col-md-6"></div>  
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                
                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; Axis Solutions </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <script src="js/jasny-bootstrap.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>
            $(document).ready(function(){
                 $(".btnupdatepasskey").hide();
                 
                 $("#oldpasskey").on("change",function(){
                     var oldkey = $(this).val();
                    $.get("adminengines/getPasskey.php",function(resp){
                        if(oldkey === resp){
                            $("#newpass").attr("readonly",false);
                            $("#confirmpass").attr("readonly",false);
                            alert("Old password correct!!");
                        }
                        else{
                            $("#newpass").attr("readonly",true);
                            $("#confirmpass").attr("readonly",true);
                            alert("Old Password incorrect!!");
                        }
                    }); 
                 });
                 
     $("#confirmpass").on("change",function(){
                            
            var InitPasskey = $("#newpass").val();
            if($(this).val()!==InitPasskey)
            {
                 $(".btnupdatepasskey").hide();
                $(".btnupdatepasskey").prop("disabled",true);
                alert("Passwords not matched!");
            }
            else{
                 $(".btnupdatepasskey").show("slow");
                 $(".btnupdatepasskey").prop("disabled",false);
            }
        });
        
         $("#newpass").on("change",function(){
                            
            var InitPasskey = $("#confirmpass").val();
            if($(this).val()!==InitPasskey)
            {
                 $(".btnupdatepasskey").hide();
                $(".btnupdatepasskey").prop("disabled",true);
                alert("New Password not matching confirmation password!");
            }
            else{
                 $(".btnupdatepasskey").show("slow");
                 $(".btnupdatepasskey").prop("disabled",false);
            }
        });
        
      
        
 $(".saving-spinner").hide();
 $(".resp").hide();
		 $('.btnupdatepasskey').click(function(){
			
	 $('.btnupdatepasskey').prop('disabled', true);
	 $('.saving-spinner').slideDown('slow');
			$.post('adminengines/updatePass.php',$(".frmUpdatePasskey").serialize(),
			function(response) {  
                            console.log(response);
                            var resp = $.parseJSON(response);
                           
                             alert(resp.msg)
			    $('.saving-spinner').slideUp('slow');
                            $(".resp").show("slow");
                            $('.btnupdatepasskey').prop('disabled', false);
				});
		return false;
		});
	
            });
            </script>
    </body>

</html>