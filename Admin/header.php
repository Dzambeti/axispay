<?php 
if(!Is_Logged_In()){
    redirect("index");
}
$excp = sizeof(getNonAttempts());
?>

<!-- Top Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part"><a class="logo" href="home.php"><b><img src="../plugins/images/axispay-white.png" alt="home" /></b></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>

                    </ul>
                   
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- End Top Navigation -->
            <!-- Left navbar-header -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <!-- input-group -->
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                                </span> </div>
                            <!-- /input-group -->
                        </li>
                        <li class="user-pro">
                            <a href="home" class="waves-effect"><img src="../plugins/images/eliteadmin-logo-dark.png" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo $_SESSION["UserCode"]; ?></span>
                            </a>

                        </li>

                        <li> <a href="Transactions" class="waves-effect"><i class="fa fa-money p-r-10"></i> <span class="hide-menu">Transactions </span></a></li>
                        <li> <a href="AttemptedTrans" class="waves-effect"><i class=" ti-credit-card p-r-10"></i> <span class="hide-menu">Attempts</span></a></li>
                        <li> <a href="UsersMaster" class="waves-effect"><i class="fa fa-users p-r-10"></i> <span class="hide-menu">Users</span></a></li>
                        <li> <a href="RatePayers" class="waves-effect"><i class="fa fa-user p-r-10"></i> <span class="hide-menu">Rate Payers</span></a></li>
                        <li> <a href="BalTyps" class="waves-effect"><i class="fa fa-money p-r-10"></i> <span class="hide-menu">Inc Codes</span></a></li>
                        <li> <a href="ActBalTypes" class="waves-effect"><i class="fa fa-credit-card-alt p-r-10"></i> <span class="hide-menu">Bal Types</span></a></li>
                        <li> <a href="SendSMS" class="waves-effect"><i class="fa fa-edit p-r-10"></i> <span class="hide-menu">Bulk SMS</span></a></li>
                        <li> <a href="viewMessages" class="waves-effect"><i class="fa fa-envelope-square p-r-10"></i> <span class="hide-menu">View SMSs</span></a></li>
                         <li> <a href="#" class="waves-effect"><i class=" ti-announcement p-r-10"></i> <span class="hide-menu">Interactions <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="uis">User Initiated Subs</a> </li>
                        </ul>
                    </li>
                        <li><a href="SyncToPromun" class="waves-effect"><i class="fa fa-android p-r-10"></i> <span class="hide-menu">Sync To Promun</span></a></li>
                        
                         <li> <a href="#" class="waves-effect"><i class="ti-bell p-r-10"></i> <span class="hide-menu">Exceptions (<?php echo $excp; ?>)<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="nullAttempts"><i class="fa fa-bug"></i><span class="hide-menu"> Null State (<?php echo $excp; ?>)</span></a> </li>
                          <!--  <li> <a href="paidAttempts"><i class="fa fa-bug"></i><span class="hide-menu"> Paid State</span></a> </li>
                            -->
                        </ul>
                    </li>
                        
                        <li> <a href="#" class="waves-effect"><i class="ti-user p-r-10"></i> <span class="hide-menu">Administration <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="CoDetails"><i class="fa fa-briefcase"></i><span class="hide-menu"> LA Details</span></a> </li>
                            <li> <a href="UpdatePassword"><i class="fa fa-lock"></i><span class="hide-menu"> Change Password</span></a> </li>
                             <li><a href="logout?logout=true" class="waves-effect"><i class="icon-logout"></i> <span class="hide-menu"> Check out</span></a></li>
                   
                        </ul>
                    </li>
                         </ul>
                </div>
            </div>
            <!-- Left navbar-header end -->
            <!-- Page Content -->

