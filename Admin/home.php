
<?php
require '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';



$SalsBy = SalesByAcc();
$AccArry = array();
foreach ($SalsBy as $Bt) {

    $Data = '{"Account": ' . '"' . $Bt["AccountNumber"] . '"' . ', "Amount": ' . $Bt["Totalu"] . '}';
    array_push($AccArry, $Data);
}
$CustArray = implode(",", $AccArry);

$GetDebts = TopTenBalances();
$BalsArray = array();
foreach ($GetDebts as $Bt) {

    $Data = '{"Account": ' . '"' . $Bt["ConsumerAccount"] . '"' . ', "Balance": ' . $Bt["ConsumerBalance"] . '}';
    array_push($BalsArray, $Data);
}
$FinBalsArray = implode(",", $BalsArray);




//sales by mom
$content = array();
$months = array();
$val = MOMSales();
foreach ($val as $vl) {
    $prd = '"'.$vl["Month"].'"';
    array_push($months, $prd);
    $amnt = $vl["Tot"];
       
    array_push($content, round($amnt,2));
}
$Months = implode(",", $months);
$amntsData = implode(",", $content);


// get credits
$getLaData = GetLADetails();
$Credits = $getLaData[0]["SMSCreds"];
$LAName = $getLaData[0]["LAName"];
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Axis Pay | Admin Dashboard</title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- morris CSS -->
        <link href="../plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>v
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4./respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <?php require 'header.php'; ?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title"><?php echo $LAName; ?></h4> </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                            <ol class="breadcrumb">
                                <li><a href="">AxisPay</a></li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!--row -->
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="white-box">
                                <div class="r-icon-stats"> <i class="ti-user bg-megna"></i>
                                    <div class="bodystate">
                                        <h4><?php echo (getPayers()); ?></h4> <span class="text-muted">Payers</span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="white-box">
                                <div class="r-icon-stats"> <i class="fa fa-envelope-square bg-success"></i>
                                    <div class="bodystate">
                                        <h4><?php echo $Credits; ?></h4> <span class="text-muted">Credits</span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="white-box">
                                <div class="r-icon-stats"> <i class="fa fa-credit-card bg-info"></i>
                                    <div class="bodystate">
                                        <h4><?php echo '$ ' .get_month_sales(); ?></h4> <span class="text-muted">MTD Receipts</span> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="white-box">
                                <div class="r-icon-stats"> <i class="fa fa-money bg-inverse"></i>
                                    <div class="bodystate">
                                        <h4><?php echo '$ ' . get_year_sales(); ?></h4> <span class="text-muted">YTD Receipts</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row -->

                    <!--row -->
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="white-box">
                                <h3 class="box-title">Payments by Customer</h3>
                                <ul class="list-inline text-center">
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>Acc #S</h5> </li>

                                </ul>
                                <div id="morris-area-chart2" style="height: 370px;"></div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                            <div class="white-box">
                                <h3 class="box-title">Top Ten Debtors</h3>
                                <ul class="list-inline text-center">
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i></h5> </li>

                                </ul>
                                <div id="morris-area-chart3" style="height: 370px;"></div>
                            </div>
                        </div>

                    </div>



                    <div class="row">
                     
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                        <div class="white-box">
                            <h3 class="box-title"> Month On Month Payments</h3>
                            <div style="height: 370px;">
                                <canvas id="chart1" ></canvas>
                            </div>
                        </div>
                    </div>
                         </div>

                    <!-- row -->


                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; Axis Solutions Africa </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!--Morris JavaScript -->
        <script src="../plugins/bower_components/raphael/raphael-min.js"></script>
        <script src="../plugins/bower_components/morrisjs/morris.js"></script>
        <!-- Sparkline chart JavaScript -->
        <script src="../plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
        <!-- jQuery peity -->
        <script src="../plugins/bower_components/peity/jquery.peity.min.js"></script>
        <script src="../plugins/bower_components/peity/jquery.peity.init.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>

        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        
          <script src="../plugins/bower_components/Chart.js/Chart.min.js"></script>

        <script>
            $(document).ready(function () {
                

                Morris.Bar({
                    element: 'morris-area-chart2',
                    data: [<?php print_r($CustArray); ?>],
                    xkey: 'Account',
                    ykeys: ['Amount'],
                    labels: ['Amount'],
                    pointSize: 0,

                    pointStrokeColors: ['#469fb4'],
                    barColors: ['#469fb4'],
                    behaveLikeLine: true,
                    gridLineColor: '#e0e0e0',
                    lineWidth: 0,
                    smooth: false,
                    hideHover: 'auto',
                    lineColors: ['#469fb4', '#01c0c8'],
                    resize: true

                });


                Morris.Bar({
                    element: 'morris-area-chart3',
                    data: [<?php print_r($FinBalsArray); ?>],
                    xkey: 'Account',
                    ykeys: ['Balance'],
                    labels: ['Amount'],
                    pointSize: 0,

                    pointStrokeColors: ['#01c0c8'],
                    barColors: ['#581845'],
                    behaveLikeLine: true,
                    gridLineColor: '#e0e0e0',
                    lineWidth: 0,
                    smooth: false,
                    hideHover: 'auto',
                    lineColors: ['#469fb4', '#01c0c8'],
                    resize: true

                });
                
               

                var ctx1 = document.getElementById("chart1").getContext("2d");
                var data1 = {
                    labels: [<?php print_r($Months); ?>],
                    datasets: [
                        
                        {
                            label: "My First dataset",
                            fillColor: "rgba(152,235,239,0.8)",
                            strokeColor: "rgba(152,235,239,0.8)",
                            pointColor: "rgba(152,235,239,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(152,235,239,1)",
                            data: [<?php print_r($amntsData); ?>]
                        }

                    ]
                };

                var chart1 = new Chart(ctx1).Line(data1, {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.005)",
                    scaleGridLineWidth: 0,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 2,
                    datasetStroke: true,
                    tooltipCornerRadius: 2,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                    responsive: true
                });


            });


        </script>
    </body>

</html>