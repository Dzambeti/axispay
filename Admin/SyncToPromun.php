<?php 
require '../DB/ODBC.php';
$AccRec = getReceiptsToSync();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title>Axispay | Sync</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- wysihtml5 CSS -->
    <link rel="stylesheet" href="plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
    <!-- Dropzone css -->
    <link href="../plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
     <link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="../css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/style.min.css" rel="stylesheet">
                              
    <!-- color CSS -->
    <link href="../css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
        <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        
      <?php  require 'header.php'; ?>
        
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Sync To Promun</h4> </div>
                   
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                             <div class="row">
                            <div class="col-lg-3 col-sm-3 col-xs-3">
                                    <a class="btn btn-block btn-primary btn-rounded SyncBalances"> Sync To ERP</a>
                            </div>
                                 
                                  
                                
                                 <div class="col-lg-5 col-sm-5 col-xs-5 slider">
                                   Please wait as the system syncs transaction to Promun......
                            </div>
                            </div> <br>
                           
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                           
                                            
                                             <th>Receipt No</th>
                                            <th>Rec Code</th>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Account Number</th>
                                            <th>Transaction Date</th>
                                           
                                            <th class="text-nowrap"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach($AccRec as $Rec){
                                            $RecNum = $Rec["TransactionID"];
                                            $OrderID  = $Rec["OrderID"]; 
                                             $Amnt  = number_format($Rec["AmountPaid"],2); 
                                              $Acc  = $Rec["AccountNumber"];  
                                              $TranscDate  = date("d M y",  strtotime($Rec["DateCreated"])); 
                                              $enc_id = base64_encode($RecNum);
                                              $recCode = $Rec["RecCode"];
                                              @$balData = incomeCodeInfoTwo($recCode);
                                                @$desc = $balData[0]["IncomeCodeDesc"];
                                                
                                                if($recCode == 'ZZ'){
                                                    $link = 'invoiceAdmin.php';
                                                }
                                                else{
                                                     $link = 'nbinvoiceAdmin.php';
                                                }
                                              
                                        ?>
                                        <tr id="<?php echo $RecNum; ?>">
                                            <td><?php echo $RecNum; ?></td>
                                            <td><?php echo $recCode; ?></td>
                                            <td><?php echo $desc; ?></td>
                                            <td>$ <?php echo $Amnt; ?></td>
                                            <td><?php echo $Acc; ?></td>
                                            <td><?php echo $TranscDate; ?></td>
                                           
                                            <td class="text-nowrap">
                                               <!--   <a href="#" class="btnSyncTransa" data-toggle="tooltip" data-original-title="Sync <?php echo $RecNum; ?> To Erp"> <i class=" ti-android"></i> </a>
                                                --><a href="<?php echo $link; ?>?RefUrl=<?php echo $enc_id; ?>" data-toggle="tooltip" data-original-title="View Invoice"> <i class=" ti-eye"></i> </a>
                                                 
                                            </td>
                                        </tr>
                                        <?php } ?>
                            
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- /.row -->
                
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; AxisPay by Axis Solutions</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bootstrap/dist/js/tether.min.js"></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
     <script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <script src="../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="../plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
    <script>
        $(document).ready(function () {
            $('.textarea_editor').wysihtml5();
             $('#myTable').DataTable();
             $(".slider").hide();
             
             $(".btnSyncTransa").click(function(ev){
                 ev.preventDefault();
                 var transID = $(this).closest("tr").attr('id');
                 $.get("adminengines/synctopro.php?state=one&recid="+transID,function(resp){
                    
                    console.log(resp);
                    var response = $.parseJSON(resp);
                    if(response.status === "ok"){
                    alert(response.msg);
                    location.reload(true);
                }
                else{
                     alert(response.msg);
                }
                 });
             });
             
             $(".SyncBalances").click(function(ev){
                 ev.preventDefault();
                  $(".slider").slideDown("slow");
                 $.get("adminengines/synctopro.php?state=two&recid=all",function(resp){
                     $(".slider").slideUp("slow");
                     console.log(resp);
                    var response = $.parseJSON(resp);
                    if(response.status === "ok"){
                         $(".slider").slideUp("slow");
                          $(".slider").html(response.msg);
                          $(".slider").slideDown("slow");
                 //   alert(response.msg);
                  //  location.reload(true);
                }
                else{
                      $(".slider").slideUp("slow");
                          $(".slider").html(response.msg);
                          $(".slider").slideDown("slow");
                }
                 });
             });
        });
    </script>
    <!-- Custom Theme JavaScript -->
    <script src="../js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>