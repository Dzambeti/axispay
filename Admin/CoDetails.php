<?php
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
$LaDetails = GetLADetails();

$LaName = $LaDetails[0]["LAName"];
$LaCode = $LaDetails[0]["LaCode"];
$LaAddress = $LaDetails[0]["Address"];
$LaVat = $LaDetails[0]["VATNum"];
$LaEmail = $LaDetails[0]["CoEmail"];
$LaOpcode = $LaDetails[0]["OpCode"];
@$LaMcno = $LaDetails[0]["McNo"];
@$IntKey = $LaDetails[0]["PaynowIntKey"];
@$intID = $LaDetails[0]["PaynowIntID"];
@$returnURL = $LaDetails[0]["returnURL"];
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>LA Details </title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php require 'header.php'; ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Update Details</h4> </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3 class="box-title m-b-0">Update Details</h3>

                                <form class="form-horizontal frmUpdateData"  method="post">
                                    <div class="form-group">
                                        <label class="col-md-12">La Name </label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="laName" value="<?php echo $LaName; ?>" name="laName" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">LA Code</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="lacode" value="<?php echo $LaCode; ?>" name="lacode" style="border-color: black;" class="input-group form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Address</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="address" value="<?php echo $LaAddress; ?>"  name="address" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Vat number</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="vatNum" value="<?php echo $LaVat; ?>"  name="vatNum" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Email</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="email" value="<?php echo $LaEmail; ?>"  name="email" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Online Operator Code</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="opcode" value="<?php echo $LaOpcode; ?>" maxlength="2"  name="opcode" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Online Machine Number (max - 3 integers)</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="number" id="mcno" value="<?php echo $LaMcno; ?>"    name="mcno" style="border-color: black;" class="form-control" > </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">Integration Key</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="opcode" value="<?php echo $IntKey; ?>"   name="intkey" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Integration ID</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="number" id="intID" value="<?php echo $intID; ?>"    name="intID" style="border-color: black;" class="form-control" > </div>
                                    </div>
                                    
                                      <div class="form-group">
                                        <label class="col-md-12">Return URL</label>
                                        <div class="col-md-6 col-lg-6">

                                            <input type="text" id="retURL" value="<?php echo $returnURL; ?>"   name="retURL" style="border-color: black;" class="form-control" > </div>
                                    </div>

                                    <div class="form-group m-b-0">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 btnUpdateCO">Update Details</button>
                                        </div>
                                    </div>

                                    <br>    <div class="ajax-loaders saving-spinner slider"> <b>...Updating Company Details...</b></div>  
                                    <div class="form-group resp col-md-6"></div>  

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Axis Solutions </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <script src="js/jasny-bootstrap.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>
            $(document).ready(function () {

                $(".saving-spinner").hide();
                $(".resp").hide();
                $('.btnUpdateCO').click(function (ev)
                {
                    ev.preventDefault();
                    $('.btnUpdateCO').prop('disabled', true);
                    $('.saving-spinner').slideDown('slow');
                    $.post('adminengines/updateCoData.php', $(".frmUpdateData").serialize(),
                            function (response) {
                                console.log(response);
                                var resp = $.parseJSON(response);

                                $('.saving-spinner').slideUp('slow');
                               $(".resp").html(resp.msg);
                                $(".resp").slideDown('slow');
                             
                                 
                                $('.btnUpdateCO').prop('disabled', false);
                            });
                    return false;
                });

            });
        </script>
    </body>

</html>