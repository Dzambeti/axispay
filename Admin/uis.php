<?php
require '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
$Forms = getAllForms();
$Ttls = array();
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
        <title>Axispay | UIS</title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- wysihtml5 CSS -->
        <link rel="stylesheet" href="plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
        <!-- Dropzone css -->
        <link href="../plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="../css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php require 'header.php'; ?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Transactions</h4> </div>







                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3 col-xs-3">
                                        <h3 class="box-title m-b-0">User initiated Subscriptions</h3>
                                    </div>

                                    <div class="col-lg-3 col-sm-3 col-xs-3">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Create Interactions</button>

                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table id="myTable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name (Label)</th>
                                                <th>Key Words</th>
                                                <th>Active Sessions</th>
                                                <th>All Participants</th>
                                                <th>Expiry Date</th>
                                                <th>Date Created</th>
                                                <th>Created By</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($Forms as $Rec) {
                                                $formref = $Rec["id"];
                                                $name = $Rec["name"];
                                                $keywords = $Rec["name"];
                                                $ExpiryDate = date("d M y", strtotime($Rec["expiry"]));
                                                $Date = date("d M y", strtotime($Rec["date_created"]));
                                                $Createdby = $Rec["creator"];
                                              $getdata =  GetFormKeywords($formref);
                                               $kywds = array();
                                        foreach($getdata as $wrd){
                                            array_push($kywds, $wrd["keyword"]);  
                                        }
                                        
                                        $Particps = (getNumOfParticipants($name));
                                                ?>
                                                <tr>
                                                    <td><?php echo $name; ?></td>
                                                    <td><?php echo implode(",",$kywds); ?></td>
                                                    <td><?php echo $Particps; ?></td>
                                                    <td><?php echo $Particps; ?></td>
                                                    <td><?php echo $ExpiryDate; ?></td>
                                                    <td><?php echo $Date; ?></td>
                                                    <td><?php echo $Createdby; ?></td>

                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <!-- /.row -->

                </div>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">User Initiated Subscriptions</h4> </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Form Name:</label>
                                        <input type="text" class="form-control" id="formname"> </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Expiry Date:</label>
                                        <input type="text" id="expdate" class="form-control mydatepicker" placeholder="mm/dd/yyyy">

                                    </div>

                                    <div class="form-group">
                                        <?php $keywrds = getUISKeywords();
                                        $kywds = array();
                                        foreach($keywrds as $wrd){
                                            array_push($kywds, $wrd["keyword"]);  
                                        }
                                        ?>
                                        <label for="message-text" class="control-label">Keywords   {<code> Do not use - <?php  echo implode(",",$kywds); ?></code>}</label>
                                        <input type="text" class="form-control" id="keywords" name="keywords" data-role="tagsinput" placeholder="add tags" /> 
                                    </div>

                                    <div class="form-group">
                                        <label for="message-text" class="control-label"><code>Subscription Probs</code></label>
                                        <table id="" class="table table-striped myta table-bordered table-form">
                                            <thead>
                                                <tr>
                                                    <th>Question</th>
                                                    <th>Table</th>
                                                    <th>Validation</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody> 
                                        </table>

                                    </div>
                                    <div class="align-right response"> .........Creating form, please wait.... </div>
                                    
                                </form>
                            </div>
                            <div class="modal-footer">
                                
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#exampleModal2">Add Form Questions</button>
                                <button type="button" id="createForm" class=" btn btn-success">Save Form</button>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade right" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="exampleModalLabel1">Questions</h4> </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Question Field:</label>
                                        <input type="text" class="form-control" id="msgbdy" placeholder="Enter question is going to be seen on mobile"> </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Choose Field</label>
                                        <select class="form-control select2" id="fieldname">
                                            <option value=""> Choose Field</option>
                                            <option value="AccNumber"> Account Number</option>
                                            <option value="AccName">Account Name</option>
                                            <option value="Email">Email</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Validation:</label>

                                        <select class="form-control select2 " id="validation">
                                            <option  value='isString()'>String</option>
                                            <option value='isIdentifier()'>Name</option>
                                            <option value='isDate()'>Is Date</option>
                                            <option value='isNumber()'>Is a number</option>

                                        </select>

                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="saveElement">Add</button>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; AxisPay by Axis Solutions</footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../bootstrap/dist/js/tether.min.js"></script>
        <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="../js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="../js/waves.js"></script>
        <script src="../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
        <script src="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
        <script src="../plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
        <script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

        <script src="../plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

        <script>
            $(document).ready(function () {
                $(".response").hide();
                $('.textarea_editor').wysihtml5();
                $('#myTable').DataTable();
                jQuery('.mydatepicker, #datepicker').datepicker();
                var counter = 0;
                $('#saveElement').click(function (event) {

                    event.preventDefault();
                    counter++;
                    var id = "field_" + Date.now();
                    var prev = "";
                    var root = false;
                    var end = true;
                    var text = "";
                    var msgbdy = "";
                    var validation = "";

                    if ($("table.table-form").find(".fields").length <= 0)
                    {
                        root = true;
                        prev = "";
                        text = $("#fieldname").val();
                        msgbdy = $("#msgbdy").val();
                        validation = $("#validation").val();
                    } else
                    {
                        $("table.table-form").find(".fields").last().attr("next", id);
                        $("table.table-form").find(".fields").last().attr("isEnd", false);
                        prev = $("table.table-form").find(".fields").last().attr("id");
                        text = $("#fieldname").val();
                        msgbdy = $("#msgbdy").val();
                        validation = $("#validation").val();
                    }
                  
                    var newRow = $('<tr style="width:50%;" id="' + id + '"><td><button class=" btn btn-default disabled"><label for="' + id + '" >' + msgbdy +
                            ' </label></button></td><td><input class="form-control fields disabled" readonly="true" id="' + id + '" validation="' + validation + '" msg="' + text + '"  msgbdy="' + msgbdy + '" isRoot="' + root + '" isEnd="' + end + '"  next ="" prev ="' + prev + '" placeholder ="' + text +
                            '"></td><td>' + validation + '</td><td><a  class="btn btn-danger btndeleted"  href="#">&times;</a></td></tr>');
                    $('table.table-form').append(newRow);
                });


                $("#createForm").click(function (e) {
                   
                    e.preventDefault();
                    var form = [];
                    
                    function FormField()
                    {
                        this.id;
                        this.next;
                        this.prev;
                        this.msg;
                        this.isRoot;
                        this.isEnd;
                        this.validations;
                    };                    
                 
                    $(".response").slideDown("slow");
                   
                    

                    $.each($("table.table-form").find(".fields"), function (index,value) {
                        var fl = new FormField();
                        fl.id = $(value).attr('id');
                        fl.next = $(value).attr('next');
                        fl.msgbdy = $(value).attr('msgbdy');
                        fl.msg = $(value).attr('msg');
                        fl.prev = $(value).attr('prev');
                        fl.isRoot = $(value).attr('isRoot');
                        fl.isEnd = $(value).attr('isEnd');
                        fl.validations = $(value).attr('validation');
                        form.push(fl);
                    });

                    console.log(form);

                    
                  $.post("adminengines/formSave.php",
                            {
                                name: $("#formname").val(),
                                keywords: $("#keywords").val(),
                                expiry_date: $("#expdate").val(),
                                dt: form
                            },
                            function (data)
                            {
                              var resp = $.parseJSON(data);
                                $(".response").slideUp("slow");
                                $(".response").html(resp.msg);
                                $(".response").slideDown("slow");
                            });
                            

                });


            });
        </script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

</html>