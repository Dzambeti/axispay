<?php
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
$userID = base64_decode($_GET["uid"]);
$UserData = GettAdminInfo($userID);
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Update Password </title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php require 'header.php'; ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Edit User</h4> </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- .row -->
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="white-box">
                                <h3 class="box-title m-b-0"></h3>

                                <form class="form-horizontal editUser"  method="post">
                                    <div class="form-group">
                                        <label class="col-md-12">Username</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="text" id="usercode" value="<?php echo $UserData[0]["UserCode"]; ?>" name="usercode" style="border-color: black;" class="form-control" placeholder="Enter username"> </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-12">User Name and Surname</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="text" id="fist" value="<?php echo $UserData[0]["Username"]; ?>" name="userfirstsec" style="border-color: black;" class="input-group form-control" placeholder="Enter First Name"> </div>
                                    </div>
                                    
                                                                       
                                    <div class="form-group">
                                        <label class="col-md-12">Phone</label>
                                        <div class="col-md-6 col-lg-6">
                                           
                                            <input type="tel" id="phone" value="<?php echo $UserData[0]["Phone"]; ?>" name="phone" style="border-color: black;" class="form-control" placeholder="Enter phone number"> </div>
                                    </div>
                                    
                                     <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 edituser">Edit User</button>
                                    </div>
                                </div>
                                    
                                  <br>    <div class="ajax-loaders saving-spinner slider"> <b>...editing new user...</b></div>  
                                       <div class="resp col-md-6"></div>  
                    
                                   
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                
                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; Axis Solutions </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <script src="js/jasny-bootstrap.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>
            $(document).ready(function(){
        
 $(".saving-spinner").hide();
 $(".resp").hide();
		 $('.edituser').click(function(){
			var id = '<?php echo $userID; ?>';
	 $('.edituser').prop('disabled', true);
	 $('.saving-spinner').slideDown('slow');
			$.post('adminengines/EditUser.php?id='+id,$(".editUser").serialize(),
			function(response) {  
                            console.log(response);
                            var resp = $.parseJSON(response);
                           
                             alert(resp.msg)
			    $('.saving-spinner').slideUp('slow');
                            $(".resp").show("slow");
                            $('.edituser').prop('disabled', false);
				});
		return false;
		});
	
            });
            </script>
    </body>

</html>