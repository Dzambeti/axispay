<?php
include_once '../DB/ODBC.php';
require 'AdminDB/DBAPI.php';
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
        <title>Upload  </title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

            <?php require 'header.php'; ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Import Rate Payers</h4> </div>

                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                            <div class="white-box">
                              <br>
                                <form class="form-horizontal" id="uploadRates" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                <h4 class=" m-b-0"> Download Sample File <a href="template/ratePayersSMS.xlsx" download="bulkSMS.xlsx" title="Download Sample File"
                                style="color: red; text-decoration:none;">Here</a></h4>       
                                </div>
                                   
                                    <div class="form-group col-4">
                                          <label>File Input for Rate Payer SMS</label>  
                                          <input type="file" id="file" name="file" class="form-control">                   
                                    </div>
                                    
                                     <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 uploadRates">Upload</button>
                                    </div>
                                </div>
                                    
                                  <br>    
                                  <div class="form-group m-b-0">
                                      <div class="ajax-loaders saving-spinner"> <b>...Uploading rate Payers...</b></div>  
                                       <div class="resp col-md-6"></div> 
                                  </div>
                    
                                   
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                
                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; Axis Solutions </footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <script src="js/jasny-bootstrap.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script>
            $(document).ready(function(){
             
       
 $(".saving-spinner").hide();
 $(".resp").hide();

		 $('.uploadRates').click(function(e){
			e.preventDefault();
            var formData =  new FormData($("form#uploadRates")[0]);
	 $('.uploadRates').prop('disabled', true);
	 $('.saving-spinner').slideDown('slow');
					
//ajax code 
        $.ajax({
                url:'adminengines/importexcel.php',
                dataType: 'text',
                cache:false,
                contentType:false,
                processData:false,
                data:formData,
                type: 'POST',
                success: function(response) {  
                            console.log(response);
                              $('.resp').html(response);
			                    $('.saving-spinner').slideUp('slow');
                            $(".resp").show("slow");
                            $('.uploadRates').prop('disabled', false);
				},
                error: function(response){
                    console.log(response);
                }
        });

		});
	
            });
            </script>
    </body>

</html>