
<?php
require 'AdminDB/ODBCAdmin.php';
require '../DB/ODBC.php';
@$Btn = $_POST["Login"];

if (isset($Btn)) {

    $AdminUserName = $_POST["AdminUsername"]; //password
    $AdminPasskey = $_POST["password"];
    // check if co details and user table are empty
    if (sizeof(GetLADetails()) <= 0) {
        $error = "Uploading LA data, please be patient.";

        $CoData = GetCoDetails();
        $LAName = $CoData[0]["cof-desc"];
        $LaCode = $CoData[0]["co-code"];
        $Address = $CoData[0]["addr1"] . "," . $CoData[0]["addr2"] . "," . $CoData[0]["addr3"];
        $VATNum = $CoData[0]["vat-reg-no"];
        if (empty($VATNum)) {
            $VATNum = $CoData[0]["link-no"];
        }
        $CentralLandLine = "";
        $CoEmail = $CoData[0]["email"];
        $CoPhone = "";
        $UploadLaData = UploadLaDet($LAName, $LaCode, $Address, $VATNum, $CentralLandLine, $CoEmail, $CoPhone);
        if ($UploadLaData["status"] == "ok") {
            $error = "Upload successful. System now uploading user data";
            $UserCode = "Axis";
            $Username = "Axis Promun";
            $Password = "00000";
            $Phone = "0773629282";
            $UplodUser = UploadUserDet($UserCode, $Username, $Password, $Phone);
            if ($UplodUser["status"] = "ok") {
                $error = "User $UserCode and  $LAName details successfully uploaded. A pilot of 30 days has been set.  ";
            } else {
                $error = "Failed to upload users. ERROR: " . $UplodUser["status"];
            }
        } else {
            $error = "Failed to upload Council data. ERROR: " . $UploadLaData["status"];
        }
    } else {
        //login
        $LaData = GetLADetails();
        $ExpiryDate = date("d M Y @ H:ia", strtotime($LaData[0]['PilotDueDate']));
        $InPilot = $LaData[0]['IsInPilot'];
        if($LaData[0]['PilotDueDate'] < date("Y-m-d H:i:s"))
        {
         $error = "Trial period has expired. Last date and time is $ExpiryDate. Contact Axis Solutions Africa for assistance, thank you.";
        }
        else {
        $login = AdminLogin($AdminUserName, $AdminPasskey);
        if ($login["status"] == "passed") {
            redirect("home");
        } else {
            $error = "Usercode or password wrong. ";
        }
        }
    }
}
?>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
        <title>Axis Pay </title>
        <!-- Bootstrap Core CSS -->
        <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="../css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../css/style.min.css" rel="stylesheet">

        <!-- color CSS -->
        <link href="../css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <div class="preloader">
            <div class="cssload-speeding-wheel">test</div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box login-sidebar">
                <div class="white-box">

                    <br><br><br><br><br><br>
                    <form class="form-horizontal form-material" id="loginform" method="post" action="">
                        <a href="javascript:void(0)" class="text-center db"><img src="../plugins/images/eliteadmin-logo-dark.png" alt="Home" />
                            <br/><img src="../plugins/images/axispay.png" alt="Home" /></a>
                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="AdminUsername" required="" placeholder="Admin Username"> </div>
                        </div>

                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password" required="" placeholder="Admin Password"> </div>
                        </div>


                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="Login">Login</button>
                            </div>
                        </div>
                        <?php
                        if (sizeof(GetLADetails()) > 0) {
                            $LaData = GetLADetails();
                            $ExpiryDate = date("d M Y @ H:ia", strtotime($LaData[0]['PilotDueDate']));
                            $InPilot = $LaData[0]['IsInPilot'];
                            if ($InPilot == 'yes') {
                                ?>
                                <div class="alert alert-warning"><?php echo 'Trial expires on ' . $ExpiryDate; ?></div>
                                <?php
                            }
                        }
                        ?>

                        <?php
                        if (isset($error)) {
                            echo '<div class="alert alert-danger">' . $error . ' </div>';
                        }
                        ?>

                    </form>
                </div>
            </div>
        </section>
        <!-- jQuery -->
        <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="../bootstrap/dist/js/tether.min.js"></script>
        <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="../js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="../js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

</html>