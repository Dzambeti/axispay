
<?php
require_once 'DB/dbapi.php';

if($_SESSION['Customer']!="")
{
	redirect("index?acc=".$_SESSION['Customer']);
}
if(isset($_GET['logout']) && $_GET['logout']=="true")
{
	logout();
	redirect("index");
}
if(!isset($_SESSION['Customer']))
{
    redirect("index");
}
