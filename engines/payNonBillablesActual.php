<?php

require ('../library/httpful.phar');
require '../DB/dbapi.php';
require '../Admin/AdminDB/mesgAPI.php';

$state = $_GET["state"];
@$attempt_id = $_GET["attempt_id"];
@$phone = $_GET["phone"];
    @$incCode = $_GET["incCode"];
 @$amount_paid = $_GET["AmntPaid"];
 @$balInfo = incomeCodeInfo($incCode);
 @$linktoAcc = $balInfo[0]["LinkedToAcc"];
 
 @$ledger = $balInfo[0]["LedgerAcc"];
 @$Acc = empty($_GET["accRef"])?$phone:$_GET["accRef"];
 
  @$coDetails = GetLADetails();
 @$IntKey = $coDetails[0]["PaynowIntKey"];
 @$IntCode = $coDetails[0]["PaynowIntID"];
 
  if($phone == "Not set")
 {
     $phone = "263773629282";
 }

 
 $ReturnUrl = "http://$_SERVER[HTTP_HOST]/AxisPay/engines/promunpay.php?state=2";
 switch ($state)
 {
     case 1: //begin transaction
        
      {
          if($incCode=="" || $incCode==NULL){
             echo '<div class="alert alert-danger"> Pay type not chosen, pick the pay type to define the payment purpose!</div>';
         }
         else if($amount_paid=="" || $amount_paid==NULL){
             echo '<div class="alert alert-danger"> Payment amount not set.</div>';
         }
         else if(empty($phone)){
           echo '<div class="alert alert-danger">Phone number can not be empty.</div>';
          
         }
         else if(empty($incCode)){
          echo '<div class="alert alert-danger">Pay code can not be empty. Please pick a code you are paying for.</div>';
             
         }
         else if($linktoAcc == TRUE){
          echo '<div class="alert alert-danger">Pay code is linked to rate payer accounts by rule. Please visit council offices to make payment. </div>';
             
         }
         else{
           $dtls["amt"] = $amount_paid;
           $dtls["acc_num"]=$ledger;
        
           
           $py = new PaynowEngine($dtls);
       
            $py->beginTransaction();
         }
     }
     break;
    case 2: // receive update from Paynow
      {
     $attempt_id = $_GET['attempt_id'];
     $recCode = base64_decode($_GET["rC"]);
     @$balData = incomeCodeInfo($recCode);
 @$desc = $balData[0]["IncomeCodeDesc"];
     $my_poll_url = GetPollUrl($attempt_id);
     
     $callback = \Httpful\Request::get($my_poll_url)->send();   //return array
     $response = ParseMyMsg($callback);

     $status = $response['status'];
    
          $Poll_attempt_id = $response['reference'];
         $attempt_det = GetOrderDet($Poll_attempt_id);  // order ID as response from paynow

     $transactionID =  substr_replace($Poll_attempt_id,"Pay",0,8); //change attempt to transaction ID
     $amount = $response['amount']; //get amount
    $acc_no = $attempt_det[0]['AccountNumber']; //getAccMNum
    $Phone =  $attempt_det[0]['phone'];
    $Email =  $attempt_det[0]['email'];
    $AccRef = $attempt_det[0]['AccRef'];
     $enc_id = base64_encode($transactionID);
    $att = LogTransID($transactionID,$Poll_attempt_id);
    
   $phonenum = $Phone;
    if($status!="Cancelled"){
    $TransSummary = "Payment of $amount for $desc has been made successfully.";

    $CreateReciept =  CreateNonBillReceipt($recCode,$transactionID,$Poll_attempt_id,$amount,$acc_no,$AccRef,$Phone,$Email);
     
     if($CreateReciept['status']=='ok'){
        echo "Payment successfully done. Wait as we redirect to your account......";
       //allocate payments
       $Bal =  GetBal($acc_no);
      
       /*
       $UltBal = $Bal[0]["Tot"];
      $GetBals = GetBalances($acc_no) ;
      foreach($GetBals as $Bal){
          $BalTyp = $Bal["BalType"];
		 $ReceiptCode =  BalanceTypeInfo($BalTyp);
		 $RecCode = $ReceiptCode[0]["ReceiptCode"];
          $AmntOut = $Bal["OnlineAmountDue"];
          $AmntPaid = round((($AmntOut/$UltBal)*$amount),4);
          $AllocatePayment =  AllocatePaymnt($AmntPaid,$acc_no,$BalTyp);
         $SetNewBal = SetNewBalance($AmntPaid,$acc_no,$BalTyp);
         UploadAlloc($BalTyp,$RecCode,$AmntPaid,$transactionID);
        
      }
        * 
        */
        $message = 'Payment of $'.$amount.' for '.$desc.' with trans ID '.$transactionID.' has been made successfully! Use ID to verify payment.';
      $la = GetLADetails();
      
      $LAcode = $la[0]["LaCode"];
       $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($message) / 160);
        if ($bal >= $credit) {
    SendSMS($phonenum,$LAcode,$message);
    reduceToSendCredits($credit);
        }
    
        // we building raw data ;
         $ReturnUrl = "http://$_SERVER[HTTP_HOST]/AxisPay/nbinvoice.php?RefUrl=".$enc_id;
         
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$ReturnUrl.'">';
  
     }
     else{
         $msg = $CreateReciept['status'];
         echo $msg;
     }
         
     
     }
     else{
         $message = 'Payment attempt of '.$amount.' for '.$desc.' has been cancelled successfully!.';
      $la = GetLADetails();
      
      $LAcode = $la[0]["LaCode"];
       $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($message) / 160);
        if ($bal >= $credit) {
    SendSMS($phonenum,$LAcode,$message);
    reduceToSendCredits($credit);
        }
    
         $actual_link = "http://$_SERVER[HTTP_HOST]/AxisPay/";
         echo "Payment cancelled with Status: ".$status.". Click <a href='$actual_link'>here</a> to pay again!";
     }
      }
      break;
     
 }
 
class PaynowEngine
{
 
    //const integration_id =5850;
    //const integration_key = "9fef7655-a18e-4cff-90bf-aeb00eb75365";
    const authemail = "";  //optional
    const initiate_transaction_url = "https://www.paynow.co.zw/interface/initiatetransaction";
    const checkout_url = "http://localhost/axispay/balance.php";
    const result_url = "http://localhost/axispay/engines/payNonBillables.php?state=2";
   var $local_order_id;
    var $txn_dtls; //POST array with txn details

    function __construct(Array $dtls) {
        $this->txn_dtls = $dtls;
        $this->local_order_id = $this->getOrderId();
    }

    function getOrderId() {
        return "Online_".time();
    }
    
function beginTransaction()
{
    global $attempt_id ;
    global $Acc;
    global $phone;
    global $incCode;
    global $ledger;
     global $IntKey;
    global $IntCode;
    
   $actual_link = "http://$_SERVER[HTTP_HOST]";
    $EncAcc = base64_encode($Acc);
    $finalIncCode = base64_encode($incCode);
    $values = array(
'resulturl' =>  PaynowEngine::result_url,
'returnurl'  => "$actual_link/AxisPay/engines/payNonBillables.php?state=2&attempt_id=".$attempt_id."&acc=$EncAcc&rC=$finalIncCode",
'reference'  => $attempt_id,
'amount'  => $this->txn_dtls["amt"],
 'account'  => $this->txn_dtls["acc_num"],       
'id' => $IntCode,
'additionalinfo'  => 'Tarrifs worth $ '.$this->txn_dtls["amt"].' paid by '.$phone,
'authemail' => "",
'status' => "Message"
);
    
    $msg = $this->createMsg($values, $IntKey);
    
    $response = \Httpful\Request::post(PaynowEngine::initiate_transaction_url)
    ->body(
    $msg
            )
	->addHeaders(array(
	'Content-Type'=>'application/json',
	'Accept'=>'application/json',
	'Content-Type' => 'application/x-www-form-urlencoded'
    ))
    ->send();
    
   
    //parse the response from Paynow
    $rslt = $this->ParseMsg( $response );
  

   if ($rslt["status"]=="Ok")
    {
       $response = \Httpful\Request::GET($rslt['pollurl'])->send();
   
      $myresult = ($this->ParseMsg($response));
      
     
     //$val_acc = $values['account'];
     $poll_url = $myresult['pollurl'];
     $amount = $myresult['amount'];
    
     $Attempt =  LogNonBillOrder($phone,$phone,$poll_url,$attempt_id,$amount,$ledger,$Acc);
     if($Attempt['status']=='ok'){
    
       echo "Redirecting to Paynow.....";
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$rslt["browserurl"].'">';

     }
     else{
         echo "ERROR: ".$Attempt['status'];
     }

    }
    else{
        echo  "Error Details : ".PHP_EOL.$rslt["error"];
    }
            
}  

function CreateHash($values, $IntKey) {
//concat string of values with integration key
    $values.=$IntKey;
    
    //create hash
    $hash = strtoupper(hash("sha512", utf8_encode($values)));
return $hash;
}

    function UpdateLocalTransaction() {
    //code to update credits and the tarnsaction details here
    }

    function finishTransaction() {
        // sh0w customer downloadable  invoice, send them the in voice to email
    }
    
    function createMsg($values, $IntKey)
    {
        $concat="";
foreach($values as $key=>$value)
{
	$concat.=$value;
}
$hash = $this-> CreateHash($concat, $IntKey);

$request_url=$this->Urlfy($values);	
$final_url = substr($request_url,0,strlen($request_url)-1)."&hash=".$hash;

return $final_url;

    }
    
    function ParseMsg($msg)
    {
        //convert to array data  
        $parts = explode("&", $msg);
        $result = array();
        foreach ($parts as $i => $value) {
            $bits = explode("=", $value, 2);
            $result[$bits[0]] = urldecode($bits[1]);
        }

        return $result;
    }

    function Urlfy($values)
    {
        $result="";
	foreach($values as $key=>$value)
{
	$result.=$key."=".urlencode($value).'&';
}
return $result;
    }
    


}
ob_end_flush();

  function ParseMyMsg($msg)
    {
        //convert to array data  
        $parts = explode("&", $msg);
        $result = array();
        foreach ($parts as $i => $value) {
            $bits = explode("=", $value, 2);
            $result[$bits[0]] = urldecode($bits[1]);
        }

        return $result;
    }
   
 
 function SendSMS($user_number,$msg_from,$message)
 {
	 if(strpos(trim($msg_from),"+") === 0)
   {
         $msg_from=substr($msg_from,1); 
       
   }
   
   if(strpos(trim($user_number),"0") === 0)
	{
		
             $user_number="263".substr($user_number,1);
		
	} 
        elseif(strpos(trim($user_number),"7") === 0)
	{
		
             $user_number="263".substr($user_number,1);
		
	} 
	else if(strpos(trim($user_number),"+") === 0)
	{
		$user_number=substr($user_number,1);
                                              
	}
        elseif (strpos(trim($user_number),"2") === 0) {
             $user_number=$user_number;
    }

 $mymessage = urlencode($message);
 $url = "http://api.bluedotsms.com/api/mt/SendSMS?user=Axis&password=1234567890&senderid=$msg_from&channel=Normal&DCS=0&flashsms=0&number=$user_number&text=$mymessage";
$response = file_get_contents($url);
$json = json_decode($response,TRUE); //generate array object from the response from the web
 return $json;

}

  
