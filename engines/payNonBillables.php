<?php

require ('../library/httpful.phar');
require '../DB/dbapi.php';
require '../Admin/AdminDB/mesgAPI.php';
require '../Ecocash/vendor/autoload.php';

$state = $_GET["state"];
@$attempt_id = $_GET["attempt_id"];
@$phone = $_GET["phone"];
    @$incCode = $_GET["incCode"];
 @$amount_paid = $_GET["AmntPaid"];
 @$balInfo = incomeCodeInfo($incCode);
 @$linktoAcc = $balInfo[0]["LinkedToAcc"];
 @$Email = $_GET["Email"];
 @$ledger = $balInfo[0]["LedgerAcc"];
 @$Acc = $_GET["accRef"];
 @$Address = $_GET["Address"];
  @$coDetails = GetLADetails();
 @$IntKey = $coDetails[0]["PaynowIntKey"];
 @$IntCode = $coDetails[0]["PaynowIntID"];
 @$retUrl = $coDetails[0]["returnURL"];
 
  
 
 //$ReturnUrl = "axispay.aximosglobal.com/engines/promunpay.php?state=2";
 switch ($state)
 {
     case 1: //begin transaction
        
      {
          if($incCode=="" || $incCode==NULL){
             echo '<div class="alert alert-danger"> Pay type not defined, pick the pay type to define the payment purpose!</div>';
         }
         else if($amount_paid=="" || $amount_paid==NULL){
             echo '<div class="alert alert-danger"> Payment amount not set.</div>';
         }
         else if($amount_paid < 0.01){
                    echo '<div class="alert alert-danger">Payment value should be more than or equal to one cent.</div>';
            }
         else if(empty($phone)){
           echo '<div class="alert alert-danger">Phone number can not be empty.</div>';
          
         }
         else if(empty($Email)){
           echo '<div class="alert alert-danger">Email Address can not be null, this is used for guest account payments.</div>';
          
         }
         else if(empty($incCode)){
          echo '<div class="alert alert-danger">Pay code can not be empty. Please pick a code you are paying for.</div>';
             
         }
          else if(empty($Acc)){
          echo '<div class="alert alert-danger">Enter Payee Name or Allocated Rate payer account number.</div>';
             
         }
          else if(empty($Address)){
          echo '<div class="alert alert-danger">Enter Your Physical Address.</div>';
             
         }
         else if($linktoAcc == TRUE){
          echo '<div class="alert alert-danger">Pay code is linked to rate payer accounts by rule. Please visit council offices to make payment. </div>';
             
         }
         else{
        
                $EncAcc = base64_encode($Acc);
                $EncAttID = base64_encode($attempt_id);
                 @$balData = incomeCodeInfo($incCode);
                @$desc = $balData[0]["IncomeCodeDesc"];
                $finalIncCode = base64_encode($incCode);
                
                
                $paynow = new Paynow\Payments\Paynow(
                        $IntCode, $IntKey, 
                        "$retUrl/engines/payNonBillables?state=2&attempt_id=" . $EncAttID . "&acc=$EncAcc&rC=$finalIncCode",
                        "$retUrl/engines/payNonBillables?state=2&attempt_id=" . $EncAttID . "&acc=$EncAcc&rC=$finalIncCode"

                        );

                $payment = $paynow->createPayment($attempt_id, $Email);

                $payment->add("$desc ($incCode)", $amount_paid);

                  $response = $paynow->send($payment);
                    if($response->success()) {
                           $url = $response->redirectUrl();
                           $poll_url = $response->pollUrl();
                           
                      $Attempt =  LogNonBillOrder($phone,$Email,$poll_url,$attempt_id,$amount_paid,$ledger,$Acc,$Address);
    
                      if ($Attempt['status'] == 'ok') {
                  echo "Redirecting to complete payment......Please wait..";
                  echo '<META HTTP-EQUIV="Refresh" Content="0; URL=' . $url . '">';  
                }
                else{
                    echo "Failed to log attempt. ERROR: ".$Attempt['status'];
                }  
                    }
                    else{
                        echo "Failed to initiate transaction.";
                    }
 
         }
     }
     break;
    case 2: // receive update from Paynow
      {
     $attempt_id = base64_decode($_GET['attempt_id']);
     $recCode = base64_decode($_GET["rC"]);
     @$balData = incomeCodeInfo($recCode);
 @$desc = $balData[0]["IncomeCodeDesc"];
 
     $my_poll_url = GetPollUrl($attempt_id);
     
     $callback = \Httpful\Request::get($my_poll_url)->send();   //return array
     $response = ParseMyMsg($callback);

     $status = $response['status'];
     if($status!="Cancelled"){
          $Poll_attempt_id = $response['reference'];
          $PaynowRef = $response["paynowreference"];
          
         $attempt_det = GetOrderDet($Poll_attempt_id);  // order ID as response from paynow

     $transactionID =  substr_replace($Poll_attempt_id,"PayNB",0,8); //change attempt to transaction ID
     $amount = $response['amount']; //get amount
    $acc_no = $attempt_det[0]['AccountNumber']; //getAccMNum
    $Phone =  $attempt_det[0]['phone'];
    $Email =  $attempt_det[0]['email'];
    $AccRef = $attempt_det[0]['AccRef'];
    $Address = $attempt_det[0]['Address'];
     $enc_id = base64_encode($transactionID);
    $att = LogTransID($transactionID,$Poll_attempt_id);
    
   $phonenum = $Phone;
   
   
    $TransSummary = "Payment of $amount for $desc has been made successfully.";

    $CreateReciept =  CreateNonBillReceipt($recCode,$transactionID,$Poll_attempt_id,$amount,$acc_no,$AccRef,$Address,$Phone,$Email,$PaynowRef);
     
     if($CreateReciept['status']=='ok'){
        echo "Payment successfully done. Wait as we redirect to your account......";
       //allocate payments
       $Bal =  GetBal($acc_no);
      
       
        $message = 'Payment of $'.$amount.' for '.$desc.' with trans ID '.$transactionID.' has been made successfully! Use ID to verify payment.';
      $la = GetLADetails();
      
      $LAcode = $la[0]["LaCode"];
       $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($message) / 160);
        if ($bal >= $credit) {
    SendSMS($phonenum,$LAcode,$message);
    reduceToSendCredits($credit);
        }
    
        // we building raw data ;
         $ReturnUrl = "$retUrl/nbinvoice?RefUrl=".$enc_id;
         
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$ReturnUrl.'">';
  
     }
     else{
         $msg = $CreateReciept['status'];
         echo $msg;
     }
         
     
     }
     else{
       //  $message = 'Payment attempt of '.$amount_paid.' for '.$desc.' has been cancelled successfully!.';
     /* $la = GetLADetails();
      
      $LAcode = $la[0]["LaCode"];
       $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($message) / 160);
        if ($bal >= $credit) {
    SendSMS($phonenum,$LAcode,$message);
    reduceToSendCredits($credit);
        }
      * 
      */
    
         $home = "$retUrl/";
         echo "Payment cancelled with Status: ".$status.". Click <a href='$home'>HERE</a> to pay again!";
     }
      }
      break;
     
 }
 
  function ParseMyMsg($msg)
    {
        //convert to array data  
        $parts = explode("&", $msg);
        $result = array();
        foreach ($parts as $i => $value) {
            $bits = explode("=", $value, 2);
            $result[$bits[0]] = urldecode($bits[1]);
        }

        return $result;
    }
   
 
 function SendSMS($user_number,$msg_from,$message)
 {
	 if(strpos(trim($msg_from),"+") === 0)
   {
         $msg_from=substr($msg_from,1); 
       
   }
   
   if(strpos(trim($user_number),"0") === 0)
	{
		
             $user_number="263".substr($user_number,1);
		
	} 
        elseif(strpos(trim($user_number),"7") === 0)
	{
		
             $user_number="263".substr($user_number,1);
		
	} 
	else if(strpos(trim($user_number),"+") === 0)
	{
		$user_number=substr($user_number,1);
                                              
	}
        elseif (strpos(trim($user_number),"2") === 0) {
             $user_number=$user_number;
    }

 $mymessage = urlencode($message);
 $url = "http://api.bluedotsms.com/api/mt/SendSMS?user=Axis&password=1234567890&senderid=$msg_from&channel=Normal&DCS=0&flashsms=0&number=$user_number&text=$mymessage";
$response = file_get_contents($url);
$json = json_decode($response,TRUE); //generate array object from the response from the web
 return $json;

}

  
