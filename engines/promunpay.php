<?php

require ('../library/httpful.phar');
require '../DB/dbapi.php';
require '../Admin/AdminDB/mesgAPI.php';
require '../Ecocash/vendor/autoload.php';

$state = $_GET["state"];
@$attempt_id = $_GET["attempt_id"];
@$Email = $_GET["email"];
@$phone = $_GET["phone"];
@$Acc = $_SESSION["Customer"];
@$amount_paid = $_GET["AmntPaid"];
@$coDetails = GetLADetails();
@$IntKey = $coDetails[0]["PaynowIntKey"];
@$IntCode = (int) $coDetails[0]["PaynowIntID"];
@$retUrl = $coDetails[0]["returnURL"];


if ($phone == "No cell or phone number") {
    $phone = "263773629282";
}

$ReturnUrl = "http://$_SERVER[HTTP_HOST]/engines/promunpay?state=2";
switch ($state) {
    case 1: //begin transaction
        {
            if ($amount_paid == "" || $amount_paid == NULL) {
                echo '<div class="alert alert-danger">Payment value not set.</div>';
            } else if($amount_paid < 0.01){
                    echo '<div class="alert alert-danger">Payment value should be more than or equal to one cent.</div>';
            }
            else if ($Email == "" || $Email == NULL || $Email == "No Mail found") {
                echo '<div class="alert alert-danger">Please provide email or identifier for your payments guest account.</div>';
            } else {

                $actual_link = "http://$_SERVER[HTTP_HOST]";
                $EncAcc = base64_encode($Acc);
                $EncAttID = base64_encode($attempt_id);

                $paynow = new Paynow\Payments\Paynow(
                        $IntCode,
                        $IntKey,
                        "$retUrl/engines/promunpay?state=2&attempt_id=$EncAttID&acc=$EncAcc",
                        "$retUrl/engines/promunpay?state=2&attempt_id=" . $attempt_id . "&acc=$EncAcc"
                );

# $paynow->setResultUrl('');
# $paynow->setReturnUrl('');

                $payment = $paynow->createPayment($attempt_id, $Email);

                $payment->add('Rates and Services (ZZ)', $amount_paid);

                  $response = $paynow->send($payment);
                    if($response->success()) {
                           $url = $response->redirectUrl();
                           $poll_url = $response->pollUrl();
                           
                      $Attempt = LogOrder($phone, $Email, $poll_url, $attempt_id, $amount_paid);
                if ($Attempt['status'] == 'ok') {
                  echo "Redirecting to complete payment......Please wait..";
                  echo '<META HTTP-EQUIV="Refresh" Content="0; URL=' . $url . '">';  
                }
                else{
                    echo "Failed to log attempt. ERROR: ".$Attempt['status'];
                }  
                    }
                    else{
                        echo "Failed to initiate transaction.";
                    }

                }
            }
            break;
            case 2: // receive update from Paynow
            {
                $attempt_id = base64_decode($_GET['attempt_id']);
                $my_poll_url = GetPollUrl($attempt_id);
               
                $callback = \Httpful\Request::get($my_poll_url)->send();   //return array
                
                $response = ParseMyMsg($callback);
            
                $status = $response['status'];
                updateAttemptStatus($status,$attempt_id);

                if ($status != "Cancelled") {
                    $Poll_attempt_id = $response['reference'];
                    $PaynowRef = $response["paynowreference"];
                   //update status
                    
                    
                    $attempt_det = GetOrderDet($Poll_attempt_id);  // order ID as response from paynow

                    $transactionID = substr_replace($Poll_attempt_id, "Rec", 0, 8); //change attempt to transaction ID
                    $amount = $response['amount']; //get amount
                    $acc_no = $attempt_det[0]['AccountNumber']; //getAccMNum
                    $Phone = $attempt_det[0]['phone'];
                    $Email = $attempt_det[0]['email'];
                    $enc_id = base64_encode($transactionID);
                    $att = LogTransID($transactionID, $Poll_attempt_id);

                    $phonenum = $Phone;
                    $TransSummary = 'Payment of ' . $amount . ' from account number ' . $acc_no . ' has been made successfully.';


                    $CreateReciept = CreateReceipt($transactionID, $Poll_attempt_id, $amount, $Phone, $Email,$PaynowRef);

                    if ($CreateReciept['status'] == 'ok') {
                        echo "Payment successfully done. Wait as we redirect to your account......";
                        //allocate payments
                        $Bal = GetBal($acc_no);

                        $message = 'Payment of $' . $amount . ' with trans ID ' . $transactionID . ' from account number ' . $acc_no . ' has been made successfully!';
                        $la = GetLADetails();

                        $LAcode = $la[0]["LaCode"];
                        $balance = getBalance();
                        $bal = $balance[0]['creds'];
                        $credit = ceil(strlen($message) / 160);
                        if ($bal >= $credit) {
                            SendSMS($phonenum, $LAcode, $message);
                            reduceToSendCredits($credit);
                        }


                        // we building raw data ;
                        $ReturnUrl = "$retUrl/invoice?RefUrl=" . $enc_id;

                        echo '<META HTTP-EQUIV="Refresh" Content="0; URL=' . $ReturnUrl . '">';
                    } else {
                        $msg = $CreateReciept['status'];
                        echo $msg;
                    }
                } else {
                    // update status 
                    $actual_link = "$retUrl/home";

                    echo "Payment cancelled with Status: " . $status . ". Click <a href='".$actual_link."'>here</a> to pay again!";
                }
            }
            break;
        }

        function SendSMS($user_number, $msg_from, $message) {
            if (strpos(trim($msg_from), "+") === 0) {
                $msg_from = substr($msg_from, 1);
            }

            if (strpos(trim($user_number), "0") === 0) {

                $user_number = "263" . substr($user_number, 1);
            } elseif (strpos(trim($user_number), "7") === 0) {

                $user_number = "263" . substr($user_number, 1);
            } else if (strpos(trim($user_number), "+") === 0) {
                $user_number = substr($user_number, 1);
            } elseif (strpos(trim($user_number), "2") === 0) {
                $user_number = $user_number;
            }

            $mymessage = urlencode($message);
            $url = "http://api.bluedotsms.com/api/mt/SendSMS?user=Axis&password=1234567890&senderid=$msg_from&channel=Normal&DCS=0&flashsms=0&number=$user_number&text=$mymessage";
            $response = file_get_contents($url);
            $json = json_decode($response, TRUE); //generate array object from the response from the web
            return $json;
        }
        
        function ParseMyMsg($msg)
    {
        //convert to array data  
        $parts = explode("&", $msg);
        $result = array();
        foreach ($parts as $i => $value) {
            $bits = explode("=", $value, 2);
            $result[$bits[0]] = urldecode($bits[1]);
        }

        return $result;
    }

    