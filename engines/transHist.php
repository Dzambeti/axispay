<?php

require '../DB/dbapi.php';
require '../Admin/AdminDB/ODBCAdmin.php';

$startDate = date("Y-m-d",strtotime($_POST["startDate"]));
$endDate = date("Y-m-d",strtotime($_POST["endDate"]));

if($startDate == "1970-01-01")
{
    echo '<div class="alert alert-danger"> Please input start date </div>';
}
else if($endDate == "1970-01-01")
{
    echo '<div class="alert alert-danger"> Please input end date </div>';
}
else if ($startDate > $endDate){
     echo '<div class="alert alert-danger"> End date can not be before start date. </div>';
}
else{
    // get transactions and input to UI
    $acc = $_SESSION["Customer"];
    $trans = getAccTransHis($acc,$startDate,$endDate);
    if(count($trans)> 0){
        
        $Details = GetConsumerDetails($acc);
        $CustomerName = $Details["data"][0]["ConsumerName"];
        $CustAddr = $Details["data"][0]["ConsumerAddress"];

$LaDetails = GetLADetails();
$Name = $LaDetails[0]["LAName"];
$LaAddress = $LaDetails[0]["Address"];
$VatNum = $LaDetails[0]["VATNum"];
   
$tranTotal = 0;
echo '<h3><b>Transaction History</b> <span class="pull-right"> from '.date("d M Y",strtotime($startDate)).' to '.date("d M Y",strtotime($endDate)).' </span></h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left"> <address>
                                                <h3> &nbsp;<b class="text-danger">'. $Name.'</b></h3>
                                                <p class="text-muted m-l-5"><b>VAT : </b>'. $VatNum.' <br>
                                                  <b>  Address : </b>'. $LaAddress.' 
                                                </p>
                                            </address> </div>
                                        <div class="pull-right text-right"> <address>

                                                <h4 class="font-bold"> '.$CustomerName.' </h4>
                                                     <p class="text-muted m-l-30"> '.$acc.',
                                                <p class="text-muted m-l-30"> '.$CustAddr.'

                                            </address> </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive m-t-40" style="clear: both;">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Date</th>
                                                        <th>Period</th>
                                                         <th>Ref</th>
                                                        <th class="text-center">Transaction Description</th>
                                                        <th class="text-right">Source/Type</th>
                                                         <th class="text-right">Amount</th>
                                                         <th class="text-right">VAT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';
            
            foreach($trans as $tran){
            
                $trdate = date("d M Y",strtotime($tran["tr-date"]));
                $Period = $tran["period"];
               
                $prog = $tran["prog"];
                if($prog == "mun116.p")
                {
                 $TransDesc = "Receipt";
                }
                else  if($prog == "MUN050.P" or $prog == "mun048.p") {
                     $TransDesc = "Bill Raised";
                }
                else{
                    $TransDesc = "Adjustment Journal";
                }
                
                $TranType = $tran["bmf-type"];
                $BalTypeList = ActBalanceTypeInfo($TranType);
                if(count($BalTypeList)>0){
                    $BalType = $BalTypeList[0]["bmfDesc"];
                }
                else{
                    $BalType = "No Type";
                }
                $Amount = $tran["amt"];
                $vat = $tran["vat-amt"];
                $ref = $tran["ref"]." - ".$prog;
                $tranTotal += $Amount;
               
          
            
                                                   echo '<tr>
                                                        <td class="text-left">'.$trdate.'</td>
                                                        <td>'.$Period.'</td>
                                                        <td>'.$ref.'</td>
                                                        <td class="text-center">'.$TransDesc.' </td>
                                                        <td class="text-right">'.$BalType.' </td>
                                                        <td class="text-right">'.$Amount.' </td>
                                                        <td class="text-right">'.$vat.' </td>
                                                    </tr>';
            }                        

                                         echo        '</tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pull-right m-t-30 text-right">
                                            
                                            <h3><b>Total :</b> '.$tranTotal.'</h3> </div>
                                       
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="text-right">
                                          
                                            <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                                        </div>
                                    </div>
                                </div>';
            
        
    }
    else{
        echo '<div class="alert alert-warning"> No transactions found for your account from the specified period. You can refresh the page to reload and clear cache. </div>';  
    }
}



