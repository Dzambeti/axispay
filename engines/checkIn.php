<?php 
require '../DB/dbapi.php';

 $Field = $_POST["accNum"];
 if(empty($Field)){
    $rslt["msg"] =  '<div class="alert alert-danger">Account Number can not be empty!</div>';  
    $rslt["status"] = "notok";
 }else{
 $GetConsumerDet = GetConsumerDetails($Field);
 if($GetConsumerDet["status"]=="ok")
 {
       $_SESSION["Customer"] = $Field;
     $Acc = base64_encode($_SESSION["Customer"]);
     $url = "home";
   
    $rslt["msg"] = $url;
    $rslt["status"] = "ok";
 }
 else{
     $error = "Account number invalid. Please verify account with your local authority.";
    $rslt["msg"] =  '<div class="alert alert-danger">'.$error.' </div>';
     $rslt["status"] = "notok";
 }
 }
 
 echo json_encode($rslt);


?>