-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2019 at 04:28 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laaxpay`
--

-- --------------------------------------------------------

--
-- Table structure for table `actualbalancetypes`
--

CREATE TABLE `actualbalancetypes` (
  `id` int(11) NOT NULL,
  `bmfType` int(11) DEFAULT NULL,
  `bmfDesc` varchar(150) DEFAULT NULL,
  `RecCode` char(10) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `SyncBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `UserCode` varchar(20) DEFAULT NULL,
  `Username` varchar(200) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attempts`
--

CREATE TABLE `attempts` (
  `id` int(11) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `poll_url` varchar(200) DEFAULT NULL,
  `order_id` varchar(200) DEFAULT NULL,
  `TransID` varchar(50) DEFAULT NULL,
  `AccountNumber` bigint(30) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `AccRef` varchar(30) DEFAULT NULL,
  `Address` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balancetypemaster`
--

CREATE TABLE `balancetypemaster` (
  `ID` int(11) NOT NULL,
  `IncCode` char(5) DEFAULT NULL,
  `IncomeCodeDesc` varchar(200) DEFAULT NULL,
  `IncType` enum('C','V') DEFAULT NULL,
  `LedgerAcc` int(15) DEFAULT NULL,
  `BalanceTypeCode` char(5) DEFAULT NULL,
  `LastSyncDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `LinkedToAcc` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customerdata`
--

CREATE TABLE `customerdata` (
  `ConsumerID` int(11) NOT NULL,
  `ConsumerAccount` bigint(20) DEFAULT NULL,
  `ConsumerName` varchar(200) DEFAULT NULL,
  `ConsumerAddress` varchar(500) DEFAULT NULL,
  `ConsumerPhone` varchar(20) DEFAULT NULL,
  `ConsumerEmail` varchar(50) DEFAULT NULL,
  `ConsumerIDNum` varchar(30) DEFAULT NULL,
  `ConsumerBalance` decimal(10,4) DEFAULT NULL,
  `ConsumerOnlineBalance` decimal(10,4) DEFAULT NULL,
  `LastSync` datetime DEFAULT CURRENT_TIMESTAMP,
  `LastSyncBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `data` varchar(10000) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `expiry` datetime NOT NULL,
  `status` varchar(11) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

CREATE TABLE `keyword` (
  `keyword_id` int(11) NOT NULL,
  `keyword` varchar(50) NOT NULL,
  `ref` int(11) NOT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lamaster`
--

CREATE TABLE `lamaster` (
  `id` int(11) NOT NULL,
  `LAName` varchar(200) DEFAULT NULL,
  `LaCode` varchar(200) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `VATNum` varchar(200) DEFAULT NULL,
  `CentralLandLine` varchar(200) DEFAULT NULL,
  `CoEmail` varchar(200) DEFAULT NULL,
  `CoPhone` varchar(200) DEFAULT NULL,
  `SMSCreds` int(11) DEFAULT NULL,
  `OpCode` char(3) DEFAULT NULL,
  `McNo` int(11) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `IsInPilot` enum('yes','no') DEFAULT NULL,
  `PilotDueDate` datetime DEFAULT NULL,
  `PaynowIntKey` varchar(100) DEFAULT NULL,
  `PaynowIntID` int(11) DEFAULT NULL,
  `returnURL` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `msg_from` varchar(30) DEFAULT NULL,
  `msg_to` varchar(30) DEFAULT NULL,
  `body` varchar(480) DEFAULT NULL,
  `sentTime` varchar(200) DEFAULT NULL,
  `receivedTime` varchar(200) DEFAULT NULL,
  `urlMsgId` varchar(100) DEFAULT NULL,
  `status` varchar(200) DEFAULT 'pending',
  `user` int(11) DEFAULT NULL,
  `Total_sms` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pos_response`
--

CREATE TABLE `pos_response` (
  `id` int(6) NOT NULL,
  `response_code` varchar(2) DEFAULT NULL,
  `response` varchar(52) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reciepts`
--

CREATE TABLE `reciepts` (
  `id` int(11) NOT NULL,
  `RecCode` char(4) DEFAULT 'ZZ',
  `TransactionID` varchar(200) DEFAULT NULL,
  `OrderID` varchar(200) DEFAULT NULL,
  `AmountPaid` decimal(10,2) DEFAULT NULL,
  `AccountNumber` int(11) DEFAULT NULL,
  `AccRef` varchar(30) DEFAULT NULL,
  `Address` varchar(400) DEFAULT NULL,
  `Phone` varchar(200) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `MunRctSyncStatus` char(10) DEFAULT NULL,
  `MunRctCtrSyncStatus` char(10) DEFAULT NULL,
  `PaynowRef` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `swipe_countdown`
--

CREATE TABLE `swipe_countdown` (
  `id` int(6) NOT NULL,
  `seconds` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uis`
--

CREATE TABLE `uis` (
  `id` int(11) NOT NULL,
  `CellNum` varchar(50) DEFAULT NULL,
  `AccNumber` int(11) DEFAULT NULL,
  `AccName` varchar(100) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `FormRef` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uisflowsession`
--

CREATE TABLE `uisflowsession` (
  `id` int(11) NOT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `formname` varchar(200) DEFAULT NULL,
  `Start_tym` datetime DEFAULT NULL,
  `Curr_Lvl` varchar(200) DEFAULT NULL,
  `Insession` tinyint(11) DEFAULT NULL,
  `Time_Done` datetime DEFAULT CURRENT_TIMESTAMP,
  `Extlid` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actualbalancetypes`
--
ALTER TABLE `actualbalancetypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attempts`
--
ALTER TABLE `attempts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `balancetypemaster`
--
ALTER TABLE `balancetypemaster`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customerdata`
--
ALTER TABLE `customerdata`
  ADD PRIMARY KEY (`ConsumerID`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `keyword`
--
ALTER TABLE `keyword`
  ADD PRIMARY KEY (`keyword_id`),
  ADD UNIQUE KEY `keyword_id` (`keyword_id`);

--
-- Indexes for table `lamaster`
--
ALTER TABLE `lamaster`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idMessage_UNIQUE` (`id`),
  ADD KEY `useidx` (`user`);

--
-- Indexes for table `pos_response`
--
ALTER TABLE `pos_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reciepts`
--
ALTER TABLE `reciepts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `swipe_countdown`
--
ALTER TABLE `swipe_countdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uis`
--
ALTER TABLE `uis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `uisflowsession`
--
ALTER TABLE `uisflowsession`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actualbalancetypes`
--
ALTER TABLE `actualbalancetypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attempts`
--
ALTER TABLE `attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `balancetypemaster`
--
ALTER TABLE `balancetypemaster`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `customerdata`
--
ALTER TABLE `customerdata`
  MODIFY `ConsumerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11175;

--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keyword`
--
ALTER TABLE `keyword`
  MODIFY `keyword_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lamaster`
--
ALTER TABLE `lamaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pos_response`
--
ALTER TABLE `pos_response`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reciepts`
--
ALTER TABLE `reciepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `swipe_countdown`
--
ALTER TABLE `swipe_countdown`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `uis`
--
ALTER TABLE `uis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `uisflowsession`
--
ALTER TABLE `uisflowsession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
