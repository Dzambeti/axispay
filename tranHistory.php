<!DOCTYPE html>
<?php
require 'DB/dbapi.php';
$Acc = $_SESSION["Customer"];
$Details = GetConsumerDetails($Acc);
$CustomerName = $Details["data"][0]["ConsumerName"];

$LaDetails = GetLADetails();
$Name = $LaDetails[0]["LAName"];
$LaAddress = $LaDetails[0]["Address"];
$VatNum = $LaDetails[0]["VATNum"];
?>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
        <title>Axispay | History</title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- wysihtml5 CSS -->
        <link rel="stylesheet" href="plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
        <!-- Dropzone css -->
        <link href="plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.min.css" rel="stylesheet">
        <link href="css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>

        <!-- color CSS -->
        <link href="css/colors/megna.css" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
<?php require 'BaseHeader.php'; ?> 
            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row bg-title">

                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Transaction History</h4> </div>
                    </div>


                    <div class="white-box">

                        <form>
                            <div class="form-inline align-items-center  ">
                                <div class="col-lg-4">
                                    <label class="sr-only" for="inlineFormInput">Start Date</label>
                                          <div class="input-group date">
                                    <input type="text" class="form-control datepicker" placeholder="Click to pick start date" id="startDate" value="<?php echo date("m/d/Y", strtotime("-30 Days")); ?>">
                                          </div>
                                          </div>
                                <div class="col-lg-4 ">
                                    <label class="sr-only" for="inlineFormInputGroup">End Date</label>
                                    <div class="input-group date">

                                        <input type="text" class="form-control datepicker" id="endDate" placeholder="Click to pick end date" value="<?php echo date("m/d/Y"); ?>">                                   
                                    </div>
                          
                                </div>

                                <div class="col-lg-4">
                                    <button type="submit" class="btn btn-primary" id="btnTransReq">Process...</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- /.row -->

                    <div class="row hide transHistDiv">
                        <div class="col-md-12">
                            <div id="printableArea" class="white-box printableArea">
                               
                            </div>
                        </div>
                    </div>
                    <!-- .row -->
                    <!-- /.row -->

                </div>
                <!-- /.container-fluid -->
                <footer class="footer text-center"> 2017 &copy; AxisPay by Axis Solutions</footer>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <script src="plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
        <script src="plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
        <script src="plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
        <script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('.textarea_editor').wysihtml5();
                $('.datepicker').datepicker();
              // $(".transHistDiv").hide();
                
                $("#btnTransReq").click(function(ev){
                    ev.preventDefault();
                    $(".printableArea").html("<div class='alert alert-info'> Processing data request, please wait...");
                     $( ".transHistDiv" ).removeClass( "hide");
                       $(".transHistDiv").show("slow");
                   var startDate = $("#startDate").val();
                   var endDate = $("#endDate").val();
                   $.post("engines/transHist.php",{
                       startDate:startDate,
                       endDate: endDate
                   },function(resp){
                        
                       $(".printableArea").html(resp);
                     $( ".transHistDiv" ).removeClass( "hide");
                       $(".transHistDiv").show("slow");
                       
                       $("#print").click(function(ev){
                           ev.preventDefault();
                            printData();
                       });
                   });
                });
                
                function printData()
{
   var divToPrint=document.getElementById("printableArea");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

            });
        </script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

</html>